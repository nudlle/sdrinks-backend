package ru.nudlik.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.core.ReactiveElasticsearchTemplate;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.data.mongodb.core.mapping.event.AbstractMongoEventListener;
import org.springframework.data.mongodb.core.mapping.event.AfterSaveEvent;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import ru.nudlik.domain.UserComment;
import ru.nudlik.domain.Recipe;
import ru.nudlik.domain.es.RecipeES;

public class CommentSaveMongoListener extends AbstractMongoEventListener<UserComment> {
    @Autowired
    private ReactiveMongoTemplate mongoOperations;
    @Autowired
    private ReactiveElasticsearchTemplate elasticsearchTemplate;

    @Override
    public void onAfterSave(AfterSaveEvent<UserComment> event) {
        UserComment savedUserComment = event.getSource();
        Criteria criteria = Criteria.where("_id").is(savedUserComment.getRecipeId());
        Query query = new Query(criteria);
        Update update = new Update();
        update.inc("commentCount");
        update.inc("rate", event.getSource().getRate());
        mongoOperations.updateFirst(query, update, Recipe.class).subscribe();

        elasticsearchTemplate.findById(String.valueOf(event.getSource().getRecipeId().hashCode()),
                RecipeES.class).subscribe(recipeES -> {
            recipeES.setCommentCount(recipeES.getCommentCount() + 1);
            recipeES.setRate(recipeES.getRate() + event.getSource().getRate());
            elasticsearchTemplate.save(recipeES).subscribe();
        });
    }
}
