package ru.nudlik.domain;

import java.io.Serializable;
import java.util.Objects;

public class ImageUrls implements Serializable {
    private static final long serialVersionUID = 887324111341L;
    private String large;
    private String medium;
    private String small;

    public ImageUrls() {
    }

    public ImageUrls(String large, String medium, String small) {
        this.large = large;
        this.medium = medium;
        this.small = small;
    }

    public String getLarge() {
        return large;
    }

    public void setLarge(String large) {
        this.large = large;
    }

    public String getMedium() {
        return medium;
    }

    public void setMedium(String medium) {
        this.medium = medium;
    }

    public String getSmall() {
        return small;
    }

    public void setSmall(String small) {
        this.small = small;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ImageUrls imageUrls = (ImageUrls) o;
        return Objects.equals(large, imageUrls.large) &&
                Objects.equals(medium, imageUrls.medium) &&
                Objects.equals(small, imageUrls.small);
    }

    @Override
    public int hashCode() {
        return Objects.hash(large, medium, small);
    }

    @Override
    public String toString() {
        return "ImageUrls{" +
                "large='" + large + '\'' +
                ", medium='" + medium + '\'' +
                ", small='" + small + '\'' +
                '}';
    }
}
