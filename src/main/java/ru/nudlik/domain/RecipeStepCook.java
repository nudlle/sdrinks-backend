package ru.nudlik.domain;

import org.bson.types.ObjectId;
import org.hibernate.validator.constraints.Length;
import org.springframework.data.annotation.Id;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.Objects;

public class RecipeStepCook implements Serializable {
    private static final long serialVersionUID = 12938701231234L;
    @Id
    private String recipeStepCookId;
    @Max(100)
    @Min(1)
    private int number;
    private String imageUrl;
    @NotBlank
    @Length(min = 10, max = 512)
    private String description;
    private String recipeId;

    public RecipeStepCook() {
        recipeStepCookId = ObjectId.get().toHexString();
    }

    public RecipeStepCook(int number, String imageUrl, String description, String recipeId) {
        this.recipeStepCookId = ObjectId.get().toHexString();
        this.number = number;
        this.imageUrl = imageUrl;
        this.description = description;
        this.recipeId = recipeId;
    }

    public String getRecipeStepCookId() {
        return recipeStepCookId;
    }

    public void setRecipeStepCookId(String recipeStepCookId) {
        this.recipeStepCookId = recipeStepCookId;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRecipeId() {
        return recipeId;
    }

    public void setRecipeId(String recipeId) {
        this.recipeId = recipeId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RecipeStepCook that = (RecipeStepCook) o;
        return number == that.number &&
                Objects.equals(recipeStepCookId, that.recipeStepCookId) &&
                Objects.equals(imageUrl, that.imageUrl) &&
                Objects.equals(description, that.description) &&
                Objects.equals(recipeId, that.recipeId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(recipeStepCookId, number, imageUrl, description, recipeId);
    }

    @Override
    public String toString() {
        return "RecipeStepCook{" +
                "recipeStepCookId='" + recipeStepCookId + '\'' +
                ", number=" + number +
                ", imageUrl='" + imageUrl + '\'' +
                ", description='" + description + '\'' +
                ", recipeId='" + recipeId + '\'' +
                '}';
    }
}
