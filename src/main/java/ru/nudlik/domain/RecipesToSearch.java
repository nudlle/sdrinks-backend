package ru.nudlik.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class RecipesToSearch implements Serializable {
    private final static long serialVersionUID = 12322022238324L;
    private String search;
    private DrinkState state;
    private DrinkType type;
    private List<DrinkTaste> tastes;
    private Boolean alcohol;

    public RecipesToSearch() {
        tastes = new ArrayList<>();
    }

    public String getSearch() {
        return search;
    }

    public void setSearch(String search) {
        this.search = search;
    }

    public DrinkState getState() {
        return state;
    }

    public void setState(DrinkState state) {
        this.state = state;
    }

    public DrinkType getType() {
        return type;
    }

    public void setType(DrinkType type) {
        this.type = type;
    }

    public List<DrinkTaste> getTastes() {
        return tastes;
    }

    public void setTastes(List<DrinkTaste> tastes) {
        this.tastes = tastes;
    }

    public Boolean getAlcohol() {
        return alcohol;
    }

    public void setAlcohol(Boolean alcohol) {
        this.alcohol = alcohol;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RecipesToSearch that = (RecipesToSearch) o;
        return alcohol == that.alcohol &&
                Objects.equals(search, that.search) &&
                state == that.state &&
                type == that.type &&
                Objects.equals(tastes, that.tastes);
    }

    @Override
    public int hashCode() {
        return Objects.hash(search, state, type, tastes, alcohol);
    }

    @Override
    public String toString() {
        return "RecipesToSearch{" +
                "text='" + search + '\'' +
                ", state=" + state +
                ", type=" + type +
                ", tastes=" + tastes +
                ", alcohol=" + alcohol +
                '}';
    }
}
