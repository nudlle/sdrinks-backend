package ru.nudlik.domain;

public enum DrinkType {
    Lemonade, Tea, Coffee, Shot, Cocktail, Milkshake, Liqueur, Unspecified
}
