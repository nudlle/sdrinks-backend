package ru.nudlik.domain;

public enum DrinkState {
    Hot, Cold, Unspecified
}
