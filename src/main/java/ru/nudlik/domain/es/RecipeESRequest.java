package ru.nudlik.domain.es;

import ru.nudlik.domain.DrinkAlcohol;
import ru.nudlik.domain.DrinkState;
import ru.nudlik.domain.DrinkTaste;
import ru.nudlik.domain.DrinkType;

import java.util.List;
import java.util.Objects;

public class RecipeESRequest {
    private DrinkState drinkState;
    private DrinkType drinkType;
    private List<DrinkTaste> drinkTastes;
    private DrinkAlcohol drinkAlcohol;

    public RecipeESRequest() {
    }

    public DrinkState getDrinkState() {
        return drinkState;
    }

    public void setDrinkState(DrinkState drinkState) {
        this.drinkState = drinkState;
    }

    public DrinkType getDrinkType() {
        return drinkType;
    }

    public void setDrinkType(DrinkType drinkType) {
        this.drinkType = drinkType;
    }

    public List<DrinkTaste> getDrinkTastes() {
        return drinkTastes;
    }

    public void setDrinkTastes(List<DrinkTaste> drinkTastes) {
        this.drinkTastes = drinkTastes;
    }

    public DrinkAlcohol getDrinkAlcohol() {
        return drinkAlcohol;
    }

    public void setDrinkAlcohol(DrinkAlcohol drinkAlcohol) {
        this.drinkAlcohol = drinkAlcohol;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RecipeESRequest that = (RecipeESRequest) o;
        return drinkAlcohol == that.drinkAlcohol &&
                drinkState == that.drinkState &&
                drinkType == that.drinkType &&
                Objects.equals(drinkTastes, that.drinkTastes);
    }

    @Override
    public int hashCode() {
        return Objects.hash(drinkState, drinkType, drinkTastes, drinkAlcohol);
    }

    @Override
    public String toString() {
        return "RecipeESRequest{" +
                "drinkState=" + drinkState +
                ", drinkType=" + drinkType +
                ", drinkTastes=" + drinkTastes +
                ", alcohol=" + drinkAlcohol +
                '}';
    }
}
