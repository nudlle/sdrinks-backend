package ru.nudlik.domain.es;


import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import ru.nudlik.domain.*;

import java.util.List;
import java.util.Objects;

@Document(indexName = "recipes", type = "recipe")
public class RecipeES {
    @Id
    private Long recipeESId;
    private String recipeId;
    private String userId;
    private String userEmail;
    private String name;
    private String secondaryName;
    private ImageUrls imageUrls;
    private DrinkState drinkState;
    private DrinkType drinkType;
    private List<DrinkTaste> drinkTastes;
    private DrinkAlcohol drinkAlcohol;
    private String hashTags;
    private long commentCount;
    private double rate;

    public RecipeES() {
    }

    public Long getRecipeESId() {
        return recipeESId;
    }

    public void setRecipeESId(Long recipeESId) {
        this.recipeESId = recipeESId;
    }

    public String getRecipeId() {
        return recipeId;
    }

    public void setRecipeId(String recipeId) {
        this.recipeId = recipeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSecondaryName() {
        return secondaryName;
    }

    public void setSecondaryName(String secondaryName) {
        this.secondaryName = secondaryName;
    }

    public DrinkState getDrinkState() {
        return drinkState;
    }

    public void setDrinkState(DrinkState drinkState) {
        this.drinkState = drinkState;
    }

    public DrinkType getDrinkType() {
        return drinkType;
    }

    public void setDrinkType(DrinkType drinkType) {
        this.drinkType = drinkType;
    }

    public List<DrinkTaste> getDrinkTastes() {
        return drinkTastes;
    }

    public void setDrinkTastes(List<DrinkTaste> drinkTastes) {
        this.drinkTastes = drinkTastes;
    }

    public DrinkAlcohol getDrinkAlcohol() {
        return drinkAlcohol;
    }

    public void setDrinkAlcohol(DrinkAlcohol drinkAlcohol) {
        this.drinkAlcohol = drinkAlcohol;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getHashTags() {
        return hashTags;
    }

    public void setHashTags(String hashTags) {
        this.hashTags = hashTags;
    }

    public ImageUrls getImageUrls() {
        return imageUrls;
    }

    public void setImageUrls(ImageUrls imageUrls) {
        this.imageUrls = imageUrls;
    }

    public long getCommentCount() {
        return commentCount;
    }

    public void setCommentCount(long commentCount) {
        this.commentCount = commentCount;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RecipeES recipeES = (RecipeES) o;
        return commentCount == recipeES.commentCount &&
                Double.compare(recipeES.rate, rate) == 0 &&
                Objects.equals(recipeESId, recipeES.recipeESId) &&
                Objects.equals(recipeId, recipeES.recipeId) &&
                Objects.equals(userId, recipeES.userId) &&
                Objects.equals(userEmail, recipeES.userEmail) &&
                Objects.equals(name, recipeES.name) &&
                Objects.equals(secondaryName, recipeES.secondaryName) &&
                Objects.equals(imageUrls, recipeES.imageUrls) &&
                drinkState == recipeES.drinkState &&
                drinkType == recipeES.drinkType &&
                Objects.equals(drinkTastes, recipeES.drinkTastes) &&
                drinkAlcohol == recipeES.drinkAlcohol &&
                Objects.equals(hashTags, recipeES.hashTags);
    }

    @Override
    public int hashCode() {
        return Objects.hash(recipeESId, recipeId, userId, userEmail, name, secondaryName, imageUrls, drinkState, drinkType, drinkTastes, drinkAlcohol, hashTags, commentCount, rate);
    }

    @Override
    public String toString() {
        return "RecipeES{" +
                "recipeESId=" + recipeESId +
                ", recipeId='" + recipeId + '\'' +
                ", userId='" + userId + '\'' +
                ", userEmail='" + userEmail + '\'' +
                ", name='" + name + '\'' +
                ", secondaryName='" + secondaryName + '\'' +
                ", imageUrls=" + imageUrls +
                ", drinkState=" + drinkState +
                ", drinkType=" + drinkType +
                ", drinkTastes=" + drinkTastes +
                ", drinkAlcohol=" + drinkAlcohol +
                ", hashTags='" + hashTags + '\'' +
                ", commentCount=" + commentCount +
                ", rate=" + rate +
                '}';
    }
}
