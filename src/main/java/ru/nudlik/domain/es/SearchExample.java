package ru.nudlik.domain.es;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

import java.util.Objects;

@Document(indexName = "search_example", type = "examples")
public class SearchExample {
    @Id
    private Long exampleId;
    private String example;

    public Long getExampleId() {
        return exampleId;
    }

    public void setExampleId(Long recipeNamesId) {
        this.exampleId = recipeNamesId;
    }

    public String getExample() {
        return example;
    }

    public void setExample(String example) {
        this.example = example;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SearchExample that = (SearchExample) o;
        return Objects.equals(exampleId, that.exampleId) &&
                Objects.equals(example, that.example);
    }

    @Override
    public int hashCode() {
        return Objects.hash(exampleId, example);
    }

    @Override
    public String toString() {
        return "SearchRecipeExamplesNames{" +
                "recipeNamesId=" + exampleId +
                ", example=" + example +
                '}';
    }
}
