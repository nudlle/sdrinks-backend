package ru.nudlik.domain;

import org.hibernate.validator.constraints.Length;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

@Document(collection = "userComment")
public class UserComment implements Serializable {
    private static long serialVersionUID = 876521342888324L;
    @Id
    private String commentId;
    @NotBlank
    private String userId;
    @NotBlank
    private String recipeId;
    @NotBlank
    private String userEmail;
    @NotBlank
    @Length(min = 5, max = 1024)
    private String comment;
    private double rate;
    @NotNull
    private Date date;

    public UserComment() {
        date = new Date();
    }

    public UserComment(String commentId, String userId, String recipeId, String userEmail, String comment, double rate, Date date) {
        this.commentId = commentId;
        this.userId = userId;
        this.recipeId = recipeId;
        this.userEmail = userEmail;
        this.comment = comment;
        this.rate = rate;
        this.date = date;
    }

    public String getCommentId() {
        return commentId;
    }

    public void setCommentId(String commentId) {
        this.commentId = commentId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getRecipeId() {
        return recipeId;
    }

    public void setRecipeId(String recipeId) {
        this.recipeId = recipeId;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserComment userComment = (UserComment) o;
        return Objects.equals(commentId, userComment.commentId) &&
                Objects.equals(userId, userComment.userId) &&
                Objects.equals(recipeId, userComment.recipeId) &&
                Objects.equals(userEmail, userComment.userEmail) &&
                Objects.equals(comment, userComment.comment) &&
                Objects.equals(date, userComment.date);
    }

    @Override
    public int hashCode() {
        return Objects.hash(commentId, userId, recipeId, userEmail, comment, date);
    }

    @Override
    public String toString() {
        return "UserComment{" +
                "commentId='" + commentId + '\'' +
                ", userId='" + userId + '\'' +
                ", recipeId='" + recipeId + '\'' +
                ", userEmail='" + userEmail + '\'' +
                ", comment='" + comment + '\'' +
                ", date=" + date +
                '}';
    }
}
