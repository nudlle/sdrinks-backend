package ru.nudlik.domain;

import org.hibernate.validator.constraints.Length;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.TextIndexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.TextScore;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.*;

@Document(collection = "recipe")
public class Recipe implements Serializable {
    private static long serialVersionUID = 123421342888324L;
    @Id
    private String recipeId;
    @NotBlank
    @Length(max = 128)
    @TextIndexed(weight = 4)
    private String name;
    @Length(max = 128)
    @TextIndexed(weight = 3)
    private String secondaryName;
    @NotBlank
    private String userId;
    @NotBlank
    @TextIndexed(weight = 2)
    private String userEmail;
    private DrinkState drinkState;
    private DrinkType drinkType;
    private List<DrinkTaste> drinkTastes;
    private List<RecipeStepCook> recipeSteps;
    private List<String> ingredients;
    private ImageUrls imageUrls;
    private double rate;
    private long commentCount;
    private Date date;
    private boolean enable;
    private DrinkAlcohol drinkAlcohol;
    @TextScore
    private Float score;
    @TextIndexed
    private String searchIndex;
    private Ad ad;
    private long viewed;
    private long liked;

    public Recipe() {
        drinkTastes = new ArrayList<>();
        recipeSteps = new ArrayList<>();
        ingredients = new ArrayList<>();
        imageUrls = new ImageUrls();
        date = new Date();
        enable = false;
        viewed = 0;
        liked = 0;
    }

    public Recipe(String userId, String userEmail) {
        this.userId = userId;
        this.userEmail = userEmail;
        this.drinkTastes = new ArrayList<>();
        this.recipeSteps = new ArrayList<>();
        this.ingredients = new ArrayList<>();
        this.rate = 0;
        this.commentCount = 0;
        this.date = new Date();
        this.imageUrls = new ImageUrls();
        this.enable = false;
        this.viewed = 0;
        this.liked = 0;
    }

    public String getRecipeId() {
        return recipeId;
    }

    public void setRecipeId(String recipeId) {
        this.recipeId = recipeId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSecondaryName() {
        return secondaryName;
    }

    public void setSecondaryName(String secondaryName) {
        this.secondaryName = secondaryName;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public DrinkType getDrinkType() {
        return drinkType;
    }

    public void setDrinkType(DrinkType drinkType) {
        this.drinkType = drinkType;
    }

    public List<DrinkTaste> getDrinkTastes() {
        return drinkTastes;
    }

    public void setDrinkTastes(List<DrinkTaste> drinkTastes) {
        this.drinkTastes = drinkTastes;
    }

    public DrinkState getDrinkState() {
        return drinkState;
    }

    public void setDrinkState(DrinkState drinkState) {
        this.drinkState = drinkState;
    }

    public List<RecipeStepCook> getRecipeSteps() {
        return recipeSteps;
    }

    public void setRecipeSteps(List<RecipeStepCook> recipeSteps) {
        this.recipeSteps = recipeSteps;
    }

    public List<String> getIngredients() {
        return ingredients;
    }

    public void setIngredients(List<String> ingredients) {
        this.ingredients = ingredients;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }

    public long getCommentCount() {
        return commentCount;
    }

    public void setCommentCount(long commentCount) {
        this.commentCount = commentCount;
    }

    public ImageUrls getImageUrls() {
        return imageUrls;
    }

    public void setImageUrls(ImageUrls imageUrls) {
        this.imageUrls = imageUrls;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public boolean isEnable() {
        return enable;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
    }

    public DrinkAlcohol getDrinkAlcohol() {
        return drinkAlcohol;
    }

    public void setDrinkAlcohol(DrinkAlcohol drinkAlcohol) {
        this.drinkAlcohol = drinkAlcohol;
    }

    public Float getScore() {
        return score;
    }

    public void setScore(Float score) {
        this.score = score;
    }

    public String getSearchIndex() {
        return searchIndex;
    }

    public void setSearchIndex(String searchIndex) {
        this.searchIndex = searchIndex;
    }

    public Ad getAd() {
        return ad;
    }

    public void setAd(Ad ad) {
        this.ad = ad;
    }

    public long getViewed() {
        return viewed;
    }

    public void setViewed(long viewed) {
        this.viewed = viewed;
    }

    public long getLiked() {
        return liked;
    }

    public void setLiked(long liked) {
        this.liked = liked;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Recipe recipe = (Recipe) o;
        return Double.compare(recipe.rate, rate) == 0 &&
                commentCount == recipe.commentCount &&
                enable == recipe.enable &&
                drinkAlcohol == recipe.drinkAlcohol &&
                viewed == recipe.viewed &&
                Objects.equals(recipeId, recipe.recipeId) &&
                Objects.equals(name, recipe.name) &&
                Objects.equals(secondaryName, recipe.secondaryName) &&
                Objects.equals(userId, recipe.userId) &&
                Objects.equals(userEmail, recipe.userEmail) &&
                drinkState == recipe.drinkState &&
                drinkType == recipe.drinkType &&
                Objects.equals(drinkTastes, recipe.drinkTastes) &&
                Objects.equals(recipeSteps, recipe.recipeSteps) &&
                Objects.equals(ingredients, recipe.ingredients) &&
                Objects.equals(imageUrls, recipe.imageUrls) &&
                Objects.equals(date, recipe.date) &&
                Objects.equals(score, recipe.score) &&
                Objects.equals(searchIndex, recipe.searchIndex) &&
                Objects.equals(ad, recipe.ad) &&
                liked == recipe.liked;
    }

    @Override
    public int hashCode() {
        return Objects.hash(recipeId, name, secondaryName, userId, userEmail, drinkState, drinkType, drinkTastes, recipeSteps, ingredients, imageUrls, rate, commentCount, date, enable, drinkAlcohol, score, searchIndex, ad, viewed, liked);
    }

    @Override
    public String toString() {
        return "Recipe{" +
                "recipeId='" + recipeId + '\'' +
                ", name='" + name + '\'' +
                ", secondaryName='" + secondaryName + '\'' +
                ", userId='" + userId + '\'' +
                ", userEmail='" + userEmail + '\'' +
                ", drinkState=" + drinkState +
                ", drinkType=" + drinkType +
                ", drinkTastes=" + drinkTastes +
                ", recipeSteps=" + recipeSteps +
                ", ingredients=" + ingredients +
                ", imageUrls=" + imageUrls +
                ", rate=" + rate +
                ", commentCount=" + commentCount +
                ", date=" + date +
                ", enable=" + enable +
                ", alcohol=" + drinkAlcohol +
                ", score=" + score +
                ", searchIndex='" + searchIndex + '\'' +
                ", ad=" + ad +
                ", recipeViewed=" + viewed +
                ", liked=" + liked +
                '}';
    }
}
