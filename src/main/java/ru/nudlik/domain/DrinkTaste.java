package ru.nudlik.domain;

public enum DrinkTaste {
    Berries, Fruit, Vegetable, Tasty, Sour, Spicy, Herb, Bitter
}
