package ru.nudlik.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.Objects;

@Document(collection = "cart")
public class Cart implements Serializable {
    private static long serialVersionUID = 1521342888324L;

    @Id
    private String cartId;
    @NotBlank
    private String userId;
    @Min(0)
    private int totalPrice;

    public Cart() {
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public int getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(int totalPrice) {
        this.totalPrice = totalPrice;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cart cart = (Cart) o;
        return totalPrice == cart.totalPrice &&
                Objects.equals(cartId, cart.cartId) &&
                Objects.equals(userId, cart.userId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(cartId, userId, totalPrice);
    }

    @Override
    public String toString() {
        return "Cart{" +
                "cartId='" + cartId + '\'' +
                ", userId='" + userId + '\'' +
                ", totalPrice=" + totalPrice +
                '}';
    }
}
