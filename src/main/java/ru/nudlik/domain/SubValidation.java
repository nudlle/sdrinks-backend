package ru.nudlik.domain;

public enum SubValidation {
    VALID, INVALID, NOT_SPECIFIED
}
