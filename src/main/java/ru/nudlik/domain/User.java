package ru.nudlik.domain;

import org.hibernate.validator.constraints.Length;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Document(collection = "user")
public class User implements Serializable {
    private static long serialVersionUID = 12380989342888324L;
    @Id
    private String userId;
    @NotBlank
    @Email
    @Length(min = 3, max = 128)
    private String email;
    @NotBlank
    @Length(min = 3, max = 128)
    private String password;
    private String avatarUrl;
    private boolean blacklist;
    private List<String> ownRecipes;
    private List<String> likedRecipes;
    private Subscription userSubscription;

    public User() {
        ownRecipes = new ArrayList<>();
        likedRecipes = new ArrayList<>();
    }

    public User(String userId, String email, String password, String avatarUrl) {
        this.userId = userId;
        this.email = email;
        this.password = password;
        this.ownRecipes = new ArrayList<>();
        this.likedRecipes = new ArrayList<>();
        this.avatarUrl = avatarUrl;
    }

    public User(User user) {
        this.userId = user.userId;
        this.email = user.email;
        this.avatarUrl = user.avatarUrl;
        this.blacklist = user.blacklist;
        this.password = user.password;
        this.ownRecipes = user.ownRecipes;
        this.likedRecipes = user.likedRecipes;
        this.userSubscription = user.userSubscription;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<String> getOwnRecipes() {
        return ownRecipes;
    }

    public void setOwnRecipes(List<String> ownRecipes) {
        this.ownRecipes = ownRecipes;
    }

    public List<String> getLikedRecipes() {
        return likedRecipes;
    }

    public void setLikedRecipes(List<String> likedRecipes) {
        this.likedRecipes = likedRecipes;
    }

    public Subscription getUserSubscription() {
        return userSubscription;
    }

    public void setUserSubscription(Subscription userSubscription) {
        this.userSubscription = userSubscription;
    }

    public boolean isBlacklist() {
        return blacklist;
    }

    public void setBlacklist(boolean blacklist) {
        this.blacklist = blacklist;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return blacklist == user.blacklist &&
                Objects.equals(userId, user.userId) &&
                Objects.equals(email, user.email) &&
                Objects.equals(password, user.password) &&
                Objects.equals(ownRecipes, user.ownRecipes) &&
                Objects.equals(likedRecipes, user.likedRecipes) &&
                Objects.equals(avatarUrl, user.avatarUrl) &&
                Objects.equals(userSubscription, user.userSubscription);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId, email, password, blacklist, ownRecipes,
                likedRecipes, avatarUrl, userSubscription);
    }

    @Override
    public String toString() {
        return "User{" +
                "userId='" + userId + '\'' +
                ", email='" + email + '\'' +
                ", avatarUrl='" + avatarUrl + '\'' +
                ", password='" + password + '\'' +
                ", blacklist=" + blacklist +
                ", ownRecipes=" + ownRecipes +
                ", likedRecipes=" + likedRecipes +
                ", userSubscription=" + userSubscription +
                '}';
    }
}
