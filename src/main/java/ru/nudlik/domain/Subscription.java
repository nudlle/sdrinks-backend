package ru.nudlik.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.time.Instant;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.Objects;

@Document(collection = "subscription")
public class Subscription implements Serializable {
    private static final long serialVersionUID = 58139493L;
    @Id
    private String subscriptionId;
    private Date endDate;
    private Double price;
    @NotBlank
    private String userId;
    private boolean activated;

    public Subscription() {
        LocalDate date = LocalDate.now(ZoneId.of("Europe/Moscow")).plusMonths(1).plusDays(1);
        endDate = java.sql.Date.valueOf(date);

//        activated = false;
    }

    public String getSubscriptionId() {
        return subscriptionId;
    }

    public void setSubscriptionId(String subscriptionId) {
        this.subscriptionId = subscriptionId;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public boolean isActivated() {
        return activated;
    }

    public void setActivated(boolean activated) {
        this.activated = activated;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Subscription that = (Subscription) o;
        return activated == that.activated &&
                Objects.equals(subscriptionId, that.subscriptionId) &&
                Objects.equals(endDate, that.endDate) &&
                Objects.equals(price, that.price) &&
                Objects.equals(userId, that.userId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(subscriptionId, endDate, price, userId, activated);
    }

    @Override
    public String toString() {
        return "Subscription{" +
                "subscriptionId='" + subscriptionId + '\'' +
                ", endDate=" + endDate +
                ", price=" + price +
                ", userId='" + userId + '\'' +
                ", activated=" + activated +
                '}';
    }
}
