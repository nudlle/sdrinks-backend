package ru.nudlik.domain;

import org.hibernate.validator.constraints.Length;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

@Document(collection = "userMessage")
public class UserMessage implements Serializable {
    private static long serialVersionUID = 763421342888324L;
    @Id
    private String userMessageId;
    @NotBlank
    private String userId;
    @Length(max = 2048)
    private String message;
    @NotBlank
    private String appVersion;
    private Date date;

    public UserMessage() {
    }

    public String getUserMessageId() {
        return userMessageId;
    }

    public void setUserMessageId(String userMessageId) {
        this.userMessageId = userMessageId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserMessage that = (UserMessage) o;
        return Objects.equals(userMessageId, that.userMessageId) &&
                Objects.equals(userId, that.userId) &&
                Objects.equals(message, that.message) &&
                Objects.equals(appVersion, that.appVersion) &&
                Objects.equals(date, that.date);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userMessageId, userId, message, appVersion, date);
    }

    @Override
    public String toString() {
        return "UserMessage{" +
                "userMessageId='" + userMessageId + '\'' +
                ", userId='" + userId + '\'' +
                ", message='" + message + '\'' +
                ", appVersion='" + appVersion + '\'' +
                ", date='" + date + '\'' +
                '}';
    }
}
