package ru.nudlik.domain;

import org.bson.types.ObjectId;
import org.hibernate.validator.constraints.Length;
import org.springframework.data.annotation.Id;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.Objects;

public class Ad implements Serializable {
    private final static long serialVersionUID = 813499837L;
    @Id
    private String adId;
    private String recipeId;
    @NotBlank
    @Length(max = 512)
    private String externalLink;
    @NotBlank
    private String imageUrl;
    @Length(max = 1024)
    private String description;
    private int stepPosition;
    private long clicked;

    public Ad() {
        adId = ObjectId.get().toHexString();
    }

    public String getAdId() {
        return adId;
    }

    public void setAdId(String adId) {
        this.adId = adId;
    }

    public String getRecipeId() {
        return recipeId;
    }

    public void setRecipeId(String recipeId) {
        this.recipeId = recipeId;
    }

    public String getExternalLink() {
        return externalLink;
    }

    public void setExternalLink(String externalLink) {
        this.externalLink = externalLink;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getStepPosition() {
        return stepPosition;
    }

    public void setStepPosition(int stepPosition) {
        this.stepPosition = stepPosition;
    }

    public long getClicked() {
        return clicked;
    }

    public void setClicked(long clicked) {
        this.clicked = clicked;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Ad ad = (Ad) o;
        return stepPosition == ad.stepPosition &&
                clicked == ad.clicked &&
                Objects.equals(adId, ad.adId) &&
                Objects.equals(recipeId, ad.recipeId) &&
                Objects.equals(externalLink, ad.externalLink) &&
                Objects.equals(imageUrl, ad.imageUrl) &&
                Objects.equals(description, ad.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(adId, recipeId, externalLink, imageUrl, description, stepPosition, clicked);
    }

    @Override
    public String toString() {
        return "Ad{" +
                "adId='" + adId + '\'' +
                ", recipeId='" + recipeId + '\'' +
                ", externalLink='" + externalLink + '\'' +
                ", imageUrl='" + imageUrl + '\'' +
                ", description='" + description + '\'' +
                ", stepPosition=" + stepPosition +
                ", adClicked=" + clicked +
                '}';
    }
}
