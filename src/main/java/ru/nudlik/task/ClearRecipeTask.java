package ru.nudlik.task;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import ru.nudlik.domain.Recipe;
import ru.nudlik.service.RecipeService;
import ru.nudlik.service.storage.FileStorageService;

import java.util.Date;

@Component
public class ClearRecipeTask {
    Logger logger = LoggerFactory.getLogger(getClass().getName());
    @Autowired
    private RecipeService recipeService;
    @Autowired
    private FileStorageService fileStorageService;

    @Scheduled(fixedDelay = 30 * 60 * 1000)
    public void clearUnusedRecipes() {
        logger.info("Try clear unused recipes. Task executed at - " + new Date());
        recipeService.getAllUnusedRecipes()
                .map(Recipe::getRecipeId)
                .doOnNext(id -> recipeService.deleteRecipe(id).subscribe())
                .doOnNext(id -> fileStorageService.deleteRecipeDir(id).subscribe())
                .subscribe();
    }
}
