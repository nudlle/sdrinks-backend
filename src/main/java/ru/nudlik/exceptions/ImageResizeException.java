package ru.nudlik.exceptions;

public class ImageResizeException extends RuntimeException {
    public ImageResizeException() {
        this("Could not resize image");
    }
    public ImageResizeException(String message) {
        super(message);
    }
}
