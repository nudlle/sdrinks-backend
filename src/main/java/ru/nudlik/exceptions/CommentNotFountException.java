package ru.nudlik.exceptions;

public class CommentNotFountException extends RuntimeException {
    public CommentNotFountException() {
        this("UserComment not found");
    }

    public CommentNotFountException(String message) {
        super(message);
    }
}
