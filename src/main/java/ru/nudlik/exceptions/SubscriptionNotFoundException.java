package ru.nudlik.exceptions;

public class SubscriptionNotFoundException extends RuntimeException {
    public SubscriptionNotFoundException() {
        this("Subscription non found");
    }
    public SubscriptionNotFoundException(String msg) {
        super(msg);
    }
}
