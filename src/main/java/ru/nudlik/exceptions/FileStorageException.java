package ru.nudlik.exceptions;

public class FileStorageException extends RuntimeException {
    public FileStorageException() {
        this("File storage exception");
    }
    public FileStorageException(String message) {
        super(message);
    }
}
