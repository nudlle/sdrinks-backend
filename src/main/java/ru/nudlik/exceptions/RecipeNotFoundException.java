package ru.nudlik.exceptions;

public class RecipeNotFoundException extends RuntimeException {
    public RecipeNotFoundException() {
        this("Recipes not found");
    }
    public RecipeNotFoundException(String message) {
        super(message);
    }
}
