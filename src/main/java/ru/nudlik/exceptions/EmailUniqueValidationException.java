package ru.nudlik.exceptions;

public class EmailUniqueValidationException extends RuntimeException {
    public EmailUniqueValidationException() {
        this("This email is already exists");
    }
    public EmailUniqueValidationException(String message) {
        super(message);
    }
}
