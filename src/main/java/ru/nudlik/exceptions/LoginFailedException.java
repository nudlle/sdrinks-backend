package ru.nudlik.exceptions;

public class LoginFailedException extends RuntimeException {
    public LoginFailedException(String message) {
        super(message);
    }
    public LoginFailedException() {
        this("Login is failed!!!");
    }
}
