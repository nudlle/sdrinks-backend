package ru.nudlik.exceptions;

public class CheckEmailHashException extends RuntimeException {
    public CheckEmailHashException() {
        this("Check code failed");
    }
    public CheckEmailHashException(String msg) {
        super(msg);
    }
}
