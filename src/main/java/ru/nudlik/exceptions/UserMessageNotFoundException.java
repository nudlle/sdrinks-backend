package ru.nudlik.exceptions;

public class UserMessageNotFoundException extends RuntimeException {
    public UserMessageNotFoundException() {
        this("AppError not found");
    }

    public UserMessageNotFoundException(String msg) {
        super(msg);
    }
}
