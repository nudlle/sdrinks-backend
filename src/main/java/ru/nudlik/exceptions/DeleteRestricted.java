package ru.nudlik.exceptions;

public class DeleteRestricted extends RuntimeException {
    public DeleteRestricted() {
        this("Delete recipe restricted");
    }
    public DeleteRestricted(String msg) {
        super(msg);
    }
}
