package ru.nudlik.exceptions;

public class ResourceLoadException extends RuntimeException {
    public ResourceLoadException() {
        this("Resource load error");
    }
    public ResourceLoadException(String message) {
        super(message);
    }
}
