package ru.nudlik.exceptions;

import reactor.core.publisher.Mono;
import ru.nudlik.response.*;

import javax.validation.ConstraintViolationException;

public class ErrorResponseFactory {
    private static ServerResponseCode getServerResponseCode(Throwable e) {
        ServerResponseCode code;
        if (e instanceof EmailUniqueValidationException) {
            code = ServerResponseCode.UNIQUE_EMAIL_EXCEPTION;
        } else if (e instanceof ConstraintViolationException) {
            code = ServerResponseCode.VALIDATION_EXCEPTION;
        } else if (e instanceof UserNotFoundException) {
            code = ServerResponseCode.USER_NOT_FOUND;
        } else if (e instanceof CommentNotFountException) {
            code = ServerResponseCode.COMMENT_NOT_FOUND;
        } else if (e instanceof RecipeNotFoundException) {
            code = ServerResponseCode.RECIPE_NOT_FOUND;
        } else if (e instanceof LoginFailedException) {
            code = ServerResponseCode.LOGIN_FAILED;
        } else if (e instanceof FileStorageException) {
            code = ServerResponseCode.FILE_UPLOAD_FAILED;
        } else if (e instanceof ResourceLoadException) {
            code = ServerResponseCode.RESOURCE_NOT_FOUND;
        } else if (e instanceof ImageResizeException) {
            code = ServerResponseCode.IMAGE_RESIZE_FAILED;
        } else if (e instanceof CheckEmailHashException) {
            code = ServerResponseCode.CHECK_EMAIL_FAILED;
        } else if (e instanceof UserMessageNotFoundException) {
            code = ServerResponseCode.USER_MESSAGE_NOT_FOUND;
        } else if (e instanceof SubscriptionNotFoundException) {
            code = ServerResponseCode.SUBSCRIPTION_NOT_FOUND;
        } else if (e instanceof SubscriptionNotValidException) {
            code = ServerResponseCode.SUBSCRIPTION_NOT_VALID;
        } else if (e instanceof DeleteRestricted) {
            code = ServerResponseCode.DELETE_RESTRICTED;
        } else {
            code = ServerResponseCode.UNKNOWN_EXCEPTION;
        }
        e.printStackTrace();
        return code;
    }
    public static Mono<UserResponse> convertUserResponse(Throwable e) {
        return Mono.just(new UserResponse(getServerResponseCode(e)));
    }

    public static Mono<CommentResponse> convertCommentResponse(Throwable e) {
        return Mono.just(new CommentResponse(getServerResponseCode(e)));
    }

    public static Mono<RecipeResponse> convertRecipeResponse(Throwable e) {
        return Mono.just(new RecipeResponse(getServerResponseCode(e)));
    }

    public static Mono<ServerResponseCode> convertToServerResponse(Throwable e) {
        return Mono.just(getServerResponseCode(e));
    }

    public static Mono<SessionTokenResponse> convertSessionTokenResponse(Throwable e) {
        return Mono.just(new SessionTokenResponse(getServerResponseCode(e)));
    }

    public static Mono<StringResponse> convertStringResponse(Throwable e) {
        return Mono.just(new StringResponse(getServerResponseCode(e)));
    }

    public static Mono<ImageUrlsResponse> convertImageUrlsResponse(Throwable e) {
        return Mono.just(new ImageUrlsResponse(getServerResponseCode(e)));
    }

    public static Mono<UserMessageResponse> convertToAppErrorResponse(Throwable e) {
        return Mono.just(new UserMessageResponse(getServerResponseCode(e)));
    }

    public static Mono<SubscriptionResponse> convertToSubscriptionResponse(Throwable e) {
        return Mono.just(new SubscriptionResponse(getServerResponseCode(e)));
    }
}
