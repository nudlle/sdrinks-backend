package ru.nudlik.exceptions;

public class SubscriptionNotValidException extends RuntimeException {
    public SubscriptionNotValidException() {
        this("Subscription date expired");
    }
    public SubscriptionNotValidException(String msg) {
        super(msg);
    }
}
