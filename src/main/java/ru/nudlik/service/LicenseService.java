package ru.nudlik.service;

import reactor.core.publisher.Mono;

public interface LicenseService {
    Mono<String> getLicense();
}
