package ru.nudlik.service.storage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.codec.multipart.FilePart;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import reactor.core.publisher.Mono;
import ru.nudlik.exceptions.FileStorageException;
import ru.nudlik.exceptions.ResourceLoadException;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Comparator;

@Service
public class FileStorageServiceImpl implements FileStorageService {
    private final Path recipeImageLocation;
    private final Path userAvatarLocation;
    private final Path licenseLocation;

    @Autowired
    public FileStorageServiceImpl(FileStorageProperties fileStorageProperties) {
        this.recipeImageLocation = Paths.get(fileStorageProperties.getRecipesUploadDir()).toAbsolutePath().normalize();
        this.userAvatarLocation = Paths.get(fileStorageProperties.getAvatarsUploadDir()).toAbsolutePath().normalize();
        this.licenseLocation = Paths.get(fileStorageProperties.getLicenseDir()).toAbsolutePath().normalize();
        try {
            Files.createDirectories(this.recipeImageLocation);
            Files.createDirectories(this.userAvatarLocation);
            Files.createDirectories(this.licenseLocation);
        } catch (Exception e) {
            throw new FileStorageException("Could not create the directory where the uploaded");
        }
    }

    private void checkFileName(String fileName) {
        if (fileName.contains("..")) {
            throw new FileStorageException("Sorry! Filename contains invalid path sequence " + fileName);
        }
    }

    @PreAuthorize("hasRole('ADMIN') or hasRole('USER')")
    @Override
    public Mono<String> storeRecipeImage(String recipeId, Mono<FilePart> file) {
        return file.map(filePart -> {
                String fileName = StringUtils.cleanPath(filePart.filename());
                checkFileName(fileName);
                try { Files.createDirectories(this.recipeImageLocation.resolve(recipeId)); }
                catch (Exception e) { throw new FileStorageException("Could not create directory"); }
                Path targetLocation = this.recipeImageLocation.resolve(recipeId + File.separator + fileName);
                filePart.transferTo(targetLocation).subscribe();
                return targetLocation.toString();
            });
    }

    @PreAuthorize("hasRole('ADMIN') or hasRole('USER')")
    @Override
    public Mono<String> storeRecipeStepImage(String recipeId, String stepId, Mono<FilePart> file) {
        return file.map(filePart -> {
            String fileName = StringUtils.cleanPath(filePart.filename());
            checkFileName(fileName);
            try {
                Files.createDirectories(this.recipeImageLocation.resolve(recipeId + File.separator + stepId));
            } catch (IOException e) { /**/ }
            Path targetLocation = this.recipeImageLocation.resolve(recipeId + File.separator +
                    stepId + File.separator + fileName);
            filePart.transferTo(targetLocation).subscribe();
            return targetLocation.toString();
        });
    }

    @PreAuthorize("hasRole('ADMIN') or hasRole('USER')")
    @Override
    public Mono<String> storeRecipeAdImage(String recipeId, String adId, Mono<FilePart> file) {
        return file.map(filePart -> {
            String fileName = StringUtils.cleanPath(filePart.filename());
            checkFileName(fileName);
            try {
                Files.createDirectories(this.recipeImageLocation.resolve(recipeId + File.separator + adId));
            } catch (IOException e) { /* */ }
            Path targetLocation = this.recipeImageLocation.resolve(recipeId + File.separator + adId +
                    File.separator + fileName);
            filePart.transferTo(targetLocation).subscribe();
            return targetLocation.toString();
        });
    }

    @PreAuthorize("hasRole('ADMIN') or hasRole('USER')")
    @Override
    public Mono<String> storeAvatarImage(String userId, Mono<FilePart> file) {
        return deleteUserDir(userId).then(file.map(filePart -> {
                            String fileName = StringUtils.cleanPath(filePart.filename());
                            checkFileName(fileName);
                            try {
                                Files.createDirectories(this.userAvatarLocation.resolve(userId));
                            } catch (IOException e) { /**/ }
                            Path targetLocation = this.userAvatarLocation.resolve(userId + File.separator + fileName);
                            filePart.transferTo(targetLocation).subscribe();
                            return targetLocation.toString();
                        }));
    }

//    @PreAuthorize("hasRole('ADMIN') or hasRole('USER')")
    @Override
    public Mono<Resource> loadAvatarAsResource(String path) {
        return Mono.just(path)
                .map(p -> {
                    try {
                        Path pp = this.userAvatarLocation.resolve(p).normalize();
                        Resource resource = new UrlResource(pp.toUri());
                        if (resource.exists()) {
                            return resource;
                        } else {
                            throw new ResourceLoadException("Image not found");
                        }
                    } catch (MalformedURLException e) {
                        throw new ResourceLoadException("Image not found " + p);
                    }
                });

    }

    @Override
    public Mono<Resource> loadRecipeAsResource(String path) {
        return Mono.just(path)
                .map(p -> {
                    try {
                        Path pp = this.recipeImageLocation.resolve(p).normalize();
                        Resource resource = new UrlResource(pp.toUri());
                        if (resource.exists()) {
                            return resource;
                        } else {
                            throw new ResourceLoadException("Image not found");
                        }
                    } catch (MalformedURLException e) {
                        throw new ResourceLoadException("Image not found " + p);
                    }
                });

    }

    @Override
    public Mono<Void> deleteRecipeDir(String recipeId) {
        return Mono.just(recipeId)
                .doOnNext(r -> {
                    try {
                        Path targetLocation = this.recipeImageLocation.resolve(r);
                        if (Files.exists(targetLocation)) {
                            Files.walk(targetLocation)
                                    .sorted(Comparator.reverseOrder())
                                    .map(Path::toFile)
                                    .forEach(p -> {
                                        try {
                                            Files.deleteIfExists(p.toPath());
                                        } catch (IOException e) {
                                            throw new FileStorageException();
                                        }
                                    });
                        }
                    } catch (IOException e) {
                        throw new FileStorageException();
                    }
                }).then();
    }

    @Override
    public Mono<Void> deleteUserDir(String userId) {
        return Mono.just(userId)
                .doOnNext(u -> {
                    try {
                        Path targetLocation = this.userAvatarLocation.resolve(u);
                        if (Files.exists(targetLocation)) {
                            Files.walk(targetLocation)
                                    .sorted(Comparator.reverseOrder())
                                    .map(Path::toFile)
                                    .forEach(p -> {
                                        try {
                                            Files.deleteIfExists(p.toPath());
                                        } catch (IOException e) {
                                            throw new FileStorageException();
                                        }
                                    });
                        }
                    } catch (IOException e) {
                        throw new FileStorageException();
                    }
                }).then();

    }

    @Override
    public Mono<Void> deleteFile(String path) {
        return Mono.just(path).doOnNext(p -> {
            try {
                Files.deleteIfExists(Paths.get(path));
            } catch (IOException e) {
                throw new FileStorageException();
            }
        }).then();
    }

    @Override
    public Mono<Void> deleteAvatar(String userId, String fileName) {
        return Mono.just(userId + File.separator + fileName)
                .map(path -> {
                    Path p = userAvatarLocation.resolve(path);
                    return deleteFile(p.toString());
                }).then();
    }

    @Override
    public Mono<Void> deleteRecipeImage(String recipeId, String fileName) {
        return Mono.just(recipeId + File.separator + fileName)
                .map(path -> {
                    Path p = recipeImageLocation.resolve(path);
                    return deleteFile(p.toString());
                }).then();
    }

    @Override
    public Mono<Void> deleteRecipeStepImage(String recipeId, String stepId, String fileName) {
        return Mono.just(recipeId + File.separator + stepId + File.separator + fileName)
                .map(path -> {
                    Path p = recipeImageLocation.resolve(path);
                    return deleteFile(p.toString());
                }).then();
    }

    @Override
    public Mono<Void> deleteRecipeAdImage(String recipeId, String adId, String fileName) {
        return Mono.just(recipeId + File.separator + adId + File.separator + fileName)
                .map(path -> {
                    Path p = recipeImageLocation.resolve(path);
                    return deleteFile(p.toString());
                }).then();
    }

    @Override
    public Mono<String> loadLicense() {
        return Mono.just("USAGE_RESTRICTIONS")
                .map(fileName -> {
                    Path p = licenseLocation.resolve(fileName);
                    String content;
                    try {
                        content = new String(Files.readAllBytes(p));
                    }
                    catch (IOException e) {
                       throw new ResourceLoadException("USER_RESTRICTIONS file not exists");
                    }
                    return content;
                });
    }
}
