package ru.nudlik.service.storage;

import org.springframework.core.io.Resource;
import org.springframework.http.codec.multipart.FilePart;
import reactor.core.publisher.Mono;

public interface FileStorageService {
    Mono<String> storeRecipeImage(String recipeId, Mono<FilePart> file);
    Mono<String> storeRecipeStepImage(String recipeId, String stepId, Mono<FilePart> file);
    Mono<String> storeRecipeAdImage(String recipeId, String adId, Mono<FilePart> file);
    Mono<String> storeAvatarImage(String userId, Mono<FilePart> file);
    Mono<Resource> loadAvatarAsResource(String path);
    Mono<Resource> loadRecipeAsResource(String path);
    Mono<Void> deleteRecipeDir(String recipeId);
    Mono<Void> deleteUserDir(String userId);
    Mono<Void> deleteFile(String path);
    Mono<Void> deleteAvatar(String userId, String fileName);
    Mono<Void> deleteRecipeImage(String recipeId, String fileName);
    Mono<Void> deleteRecipeStepImage(String recipeId, String stepId, String fileName);
    Mono<Void> deleteRecipeAdImage(String recipeId, String adId, String fileName);
    Mono<String> loadLicense();
}
