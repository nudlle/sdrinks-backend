package ru.nudlik.service.storage;

public interface ImageResizeService {
    String resize(String inputPath, int scaledWidth, int scaledHeight);
    String resize(String inputPath, double percent);
}
