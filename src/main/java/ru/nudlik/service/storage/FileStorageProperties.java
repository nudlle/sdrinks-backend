package ru.nudlik.service.storage;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "file")
public class FileStorageProperties {
    private String avatarsUploadDir;
    private String recipesUploadDir;
    private String licenseDir;

    public String getAvatarsUploadDir() {
        return avatarsUploadDir;
    }

    public void setAvatarsUploadDir(String avatarsUploadDir) {
        this.avatarsUploadDir = avatarsUploadDir;
    }

    public String getRecipesUploadDir() {
        return recipesUploadDir;
    }

    public void setRecipesUploadDir(String recipesUploadDir) {
        this.recipesUploadDir = recipesUploadDir;
    }

    public String getLicenseDir() {
        return licenseDir;
    }

    public void setLicenseDir(String licenseDir) {
        this.licenseDir = licenseDir;
    }
}
