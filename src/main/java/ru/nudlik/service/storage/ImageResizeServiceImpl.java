package ru.nudlik.service.storage;

import org.springframework.stereotype.Service;
import ru.nudlik.exceptions.ImageResizeException;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

@Service
public class ImageResizeServiceImpl implements ImageResizeService {

    @Override
    public String resize(String inputPath, int scaledWidth, int scaledHeight) {
        try {
            File inputFile = new File(inputPath);
            BufferedImage inputImage = ImageIO.read(inputFile);
            BufferedImage outputImage = new BufferedImage(scaledWidth, scaledHeight, inputImage.getType());

            Graphics2D g2d = outputImage.createGraphics();
            g2d.drawImage(inputImage, 0, 0, scaledWidth, scaledHeight, null);
            g2d.dispose();

            String formatName = inputPath.substring(inputPath.lastIndexOf(".") + 1);
            String outputPath = inputPath.substring(0, inputPath.lastIndexOf(".")) + "_" +
                    scaledWidth + "_" + scaledHeight + "." + formatName;
            ImageIO.write(outputImage, formatName, new File(outputPath));
            return outputPath;
        } catch (IOException e) {
            throw new ImageResizeException("Error resize image");
        }
    }

    @Override
    public String resize(String inputPath, double percent) {
        try {
            File inputFile = new File(inputPath);
            BufferedImage inputImage = ImageIO.read(inputFile);
            int scaledWidth = (int) (inputImage.getWidth() * percent);
            int scaledHeight = (int) (inputImage.getHeight() * percent);
            return resize(inputPath, scaledWidth, scaledHeight);
        } catch (IOException e) {
            throw new ImageResizeException("Error resize image");
        }
    }
}
