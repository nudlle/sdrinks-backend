package ru.nudlik.service;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.nudlik.domain.SubValidation;
import ru.nudlik.domain.Subscription;

public interface SubscriptionService {
    Mono<Subscription> createSubscription(Subscription subscription);
    Mono<Void> deleteSubscription(String subId);
    Flux<Subscription> getSubscriptions(String userId);
    Mono<Void> deleteAllByUserId(String userId);
    Flux<Subscription> getValidSubscriptions(String userId);
    Mono<Void> activateSubscription(String id);
    Mono<SubValidation> checkSubscription(String userId);
}
