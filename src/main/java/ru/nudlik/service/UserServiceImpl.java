package ru.nudlik.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;
import ru.nudlik.domain.Subscription;
import ru.nudlik.domain.User;
import ru.nudlik.exceptions.UserNotFoundException;
import ru.nudlik.repository.UserRepository;
import ru.nudlik.validator.UserUniqueFieldsValidator;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository repository;

    @Override
    public Mono<User> createUser(User user) {
        UserUniqueFieldsValidator validator = new UserUniqueFieldsValidator(repository);
        return validator.validateMono(user)
                .flatMap(ac -> repository.save(ac));
    }

    @PreAuthorize("hasRole('ADMIN')")
    @Override
    public Mono<Void> deleteUser(String userId) {
        return repository.deleteById(userId);
    }

    @Override
    public Mono<User> getUser(String userId) {
        return repository.findById(userId)
                .switchIfEmpty(Mono.error(new UserNotFoundException("User not found by id: " + userId)));
    }

    @Override
    public Mono<Void> updateUserPassword(String userId, String newPassword) {
        return repository.updateFields(userId, new String[]{"password"}, new Object[] {newPassword});
    }

    @Override
    public Mono<Void> updateUserAvatar(String userId, String avatarPath) {
        return repository.updateFields(userId, new String[]{"avatarUrl"}, new Object[]{avatarPath});
    }

    @PreAuthorize("hasRole('ADMIN')")
    @Override
    public Mono<Void> putUserToBlacklist(String userId, boolean addOrDelete) {
        return repository.updateFields(userId, new String[]{"blacklist"}, new Object[]{addOrDelete});
    }

    @Override
    public Mono<User> getUserByEmail(String email) {
        return repository.findByEmail(email)
                .switchIfEmpty(Mono.error(new UserNotFoundException("User not found by email: " + email)));
    }

    @Override
    public Mono<Void> putLikedRecipe(String userId, String recipeId) {
        return repository.pushLikedRecipe(userId, recipeId);
    }

    @Override
    public Mono<Void> deleteLikedRecipe(String userId, String recipeId) {
        return repository.pullLikedRecipe(userId, recipeId);
    }

    @Override
    public Mono<Void> putOwnRecipe(String userId, String recipeId) {
        return repository.pushOwnRecipe(userId, recipeId);
    }

    @Override
    public Mono<Void> deleteOwnRecipe(String userId, String recipeId) {
        return repository.pullOwnRecipe(userId, recipeId);
    }

    @Override
    public Mono<Void> putSubscription(Subscription subscription) {
        return repository.putSubscription(subscription);
    }

    @Override
    public Mono<Void> activateSubscription(String id) {
        return repository.activateSubscription(id);
    }
}
