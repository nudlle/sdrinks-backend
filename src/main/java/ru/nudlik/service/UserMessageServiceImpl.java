package ru.nudlik.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.nudlik.domain.UserMessage;
import ru.nudlik.exceptions.UserMessageNotFoundException;
import ru.nudlik.repository.UserMessageRepository;

import java.util.Date;

@Service
public class UserMessageServiceImpl implements UserMessageService {
    @Autowired
    private UserMessageRepository repository;

    @Override
    public Mono<UserMessage> getUserMessageById(String userMessageId) {
        return repository.findById(userMessageId)
                .switchIfEmpty(Mono.error(new UserMessageNotFoundException()));
    }

    @Override
    public Mono<UserMessage> createUserMessage(UserMessage request) {
        request.setDate(new Date());
        return repository.save(request);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @Override
    public Mono<Void> deleteUserMessage(String appErrorRequestId) {
        return repository.deleteById(appErrorRequestId);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @Override
    public Flux<UserMessage> getUserMessageByDate(Date date) {
        return repository.findAllByDate(date)
                .switchIfEmpty(Flux.error(new UserMessageNotFoundException()));
    }

    @Override
    public Flux<UserMessage> getUserMessageByUser(String userId) {
        return repository.findAllByUserId(userId)
                .switchIfEmpty(Flux.error(new UserMessageNotFoundException()));
    }

    @Override
    public Flux<UserMessage> getUserMessageByAppVersion(String appVersion) {
        return repository.findAllByAppVersion(appVersion)
                .switchIfEmpty(Flux.error(new UserMessageNotFoundException()));
    }
}
