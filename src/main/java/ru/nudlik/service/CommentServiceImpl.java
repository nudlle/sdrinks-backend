package ru.nudlik.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.nudlik.domain.UserComment;
import ru.nudlik.exceptions.CommentNotFountException;
import ru.nudlik.repository.CommentRepository;
import ru.nudlik.response.ServerResponseCode;

import java.util.Date;

@Service
public class CommentServiceImpl implements CommentService {
    @Autowired
    private CommentRepository repository;

    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    @Override
    public Mono<UserComment> createComment(UserComment userComment) {
        return repository.save(userComment);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @Override
    public Mono<Void> deleteComment(String commentId) {
        return repository.deleteById(commentId);
    }

    @Override
    public Flux<UserComment> getComments(String recipeId, int page, int size) {
        return repository.retrieveCommentPageableWithRecipeId(recipeId,
                PageRequest.of(page, size, Sort.by(Sort.Direction.DESC, "date")))
                .switchIfEmpty(Flux.error(new CommentNotFountException()));
    }

    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    @Override
    public Mono<ServerResponseCode> isAlreadyPosted(String recipeId, String userId) {
        return repository.findByRecipeIdAndUserId(recipeId, userId)
                .map(c -> ServerResponseCode.COMMENT_ALREADY_POSTED)
                .switchIfEmpty(Mono.just(ServerResponseCode.OK_RESPONSE));
    }
}
