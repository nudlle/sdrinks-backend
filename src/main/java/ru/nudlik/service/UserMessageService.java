package ru.nudlik.service;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.nudlik.domain.UserMessage;

import java.util.Date;

public interface UserMessageService {
    Mono<UserMessage> getUserMessageById(String userMessageId);
    Mono<UserMessage> createUserMessage(UserMessage request);
    Mono<Void> deleteUserMessage(String userMessageId);
    Flux<UserMessage> getUserMessageByDate(Date date);
    Flux<UserMessage> getUserMessageByUser(String userId);
    Flux<UserMessage> getUserMessageByAppVersion(String appVersion);
}
