package ru.nudlik.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.nudlik.domain.Recipe;
import ru.nudlik.domain.SubValidation;
import ru.nudlik.domain.Subscription;
import ru.nudlik.exceptions.SubscriptionNotFoundException;
import ru.nudlik.exceptions.SubscriptionNotValidException;
import ru.nudlik.repository.SubscriptionRepository;

import java.util.Date;

@Service
public class SubscriptionServiceImpl implements SubscriptionService {
    @Autowired
    private SubscriptionRepository repository;

    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    @Override
    public Mono<Subscription> createSubscription(Subscription subscription) {
        return repository.save(subscription);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @Override
    public Mono<Void> deleteSubscription(String subId) {
        return repository.deleteById(subId);
    }

    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    @Override
    public Flux<Subscription> getSubscriptions(String userId) {
        return repository.findAllByUserIdOrderByEndDate(userId)
                .switchIfEmpty(Flux.error(new SubscriptionNotFoundException()));
    }

    @PreAuthorize("hasRole('ADMIN')")
    @Override
    public Mono<Void> deleteAllByUserId(String userId) {
        return repository.deleteByUserId(userId).then();
    }

    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    @Override
    public Flux<Subscription> getValidSubscriptions(String userId) {
        return repository.retrieveEnableValues(userId, new Date())
                .switchIfEmpty(Flux.error(new SubscriptionNotFoundException()));
    }

    @PreAuthorize("hasRole('ADMIN')")
    @Override
    public Mono<Void> activateSubscription(String id) {
        return repository.updateFields(id, new String[]{"activated"}, new Object[]{true});
    }

    @Override
    public Mono<SubValidation> checkSubscription(String userId) {
        return repository.retrieveEnableValues(userId, new Date())
                .filter(Subscription::isActivated)
                .next().map(s -> SubValidation.VALID)
                .switchIfEmpty(Mono.just(SubValidation.INVALID));
    }
}
