package ru.nudlik.service;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.nudlik.domain.UserComment;
import ru.nudlik.response.ServerResponseCode;

public interface CommentService {
    Mono<UserComment> createComment(UserComment userComment);
    Mono<Void> deleteComment(String commentId);
    Flux<UserComment> getComments(String recipeId, int page, int size);
    Mono<ServerResponseCode> isAlreadyPosted(String recipeId, String userId);
}
