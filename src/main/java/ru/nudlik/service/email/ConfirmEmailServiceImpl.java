package ru.nudlik.service.email;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;
import ru.nudlik.response.ServerResponseCode;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Service
public class ConfirmEmailServiceImpl implements ConfirmEmailService {
    private final Map<String, String> cash = new HashMap<>();

    @Autowired
    private EmailService emailService;

    private void putToCash(String email, String random) {
        cash.put(email, random);
    }

    private String getFromCash(String email) {
        return cash.get(email);
    }

    private void removeFromCash(String email) {
        cash.remove(email);
    }

    private void sendRandomToEmail(String email, String random) {
        new Thread(() -> emailService.sendEmail(htmlContent(email, random))).start();
    }

    @Override
    public Mono<ServerResponseCode> generateRandomString(String email) {
        return Mono.just(UUID.randomUUID().toString())
                .doOnNext(random -> sendRandomToEmail(email, random))
                .doOnNext(random -> putToCash(email, random))
                .thenReturn(ServerResponseCode.OK_RESPONSE);

    }

    @Override
    public Mono<Boolean> checkEmail(String email, String randomString) {
        String cashed = getFromCash(email);
        if (cashed != null && cashed.equals(randomString)) {
            removeFromCash(email);
            return Mono.just(true);
        }
        return Mono.just(false);
    }

    private Mail htmlContent(String to, String random) {
        Mail mail = new Mail();
        mail.setFrom("no-reply@s-drinks.ru");
        mail.setTo(to);
        mail.setSubject("Verify your email code");
        mail.setContent("Your verify generated code: " + random);
        return mail;
    }
}
