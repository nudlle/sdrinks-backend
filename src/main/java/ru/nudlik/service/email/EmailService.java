package ru.nudlik.service.email;

public interface EmailService {
    void sendEmail(Mail mail);
}
