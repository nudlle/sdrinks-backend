package ru.nudlik.service.email;

import reactor.core.publisher.Mono;
import ru.nudlik.response.ServerResponseCode;

public interface ConfirmEmailService {
    Mono<ServerResponseCode> generateRandomString(String email);
    Mono<Boolean> checkEmail(String email, String randomString);
}
