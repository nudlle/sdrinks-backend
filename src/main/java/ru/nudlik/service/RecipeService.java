package ru.nudlik.service;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.nudlik.domain.DrinkType;
import ru.nudlik.domain.ImageUrls;
import ru.nudlik.domain.Recipe;
import ru.nudlik.domain.RecipesToSearch;

public interface RecipeService {
    Mono<Recipe> createRecipe(Recipe recipe);
    Mono<Void> deleteRecipe(String recipeId);
    Mono<Recipe> updateRecipe(Recipe recipe);
    Mono<Recipe> getRecipeById(String recipeId);
    Flux<Recipe> getRecipes(int page, int size);
    Flux<Recipe> getRecipesByUserId(String userId, int page, int size);
    Flux<Recipe> getRecipesByDrinkType(DrinkType drinkType, int page, int size);
    Flux<Recipe> getRecipesByUserEmail(String userEmail, int page, int size);
    Mono<Void> updateRecipeImages(String recipeId, ImageUrls urls);
    Mono<Void> updateRecipeStepImage(String stepId, String url);
    Mono<Void> updateRecipeAdImage(String adId, String url);
    Mono<Void> activateRecipe(String recipeId);
    Mono<Void> updateRecipeStepsRecipeId(String stepId, String recipeId);
    Mono<Void> updateRecipeAdRecipeId(String adId, String recipeId);
    Flux<Recipe> searchRecipe(RecipesToSearch recipesToSearch, int page, int size);
    Mono<Void> incRecipeViewed(String recipeId);
    Mono<Void> incRecipeAdClicked(String adId);
    Mono<Void> incRecipeLiked(String recipeId);
    Mono<Void> decRecipeLiked(String recipeId);
    Mono<Boolean> isUserRecipe(String userId, String recipeId);
    Flux<Recipe> getAllUnusedRecipes();
}
