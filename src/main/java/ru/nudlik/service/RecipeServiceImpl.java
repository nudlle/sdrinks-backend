package ru.nudlik.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.nudlik.domain.*;
import ru.nudlik.exceptions.RecipeNotFoundException;
import ru.nudlik.repository.RecipeRepository;

import java.util.Date;

@Service
public class RecipeServiceImpl implements RecipeService {
    @Autowired
    private RecipeRepository repository;

    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    @Override
    public Mono<Recipe> createRecipe(Recipe recipe) {
        recipe.setEnable(false);
        return repository.save(recipe);
    }

    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    @Override
    public Mono<Void> deleteRecipe(String recipeId) {
        return repository.deleteById(recipeId);
    }

    @Override
    public Mono<Boolean> isUserRecipe(String userId, String recipeId) {
        return repository.findById(recipeId).map(recipe -> recipe.getUserId().equals(userId));
    }

    @Override
    public Mono<Recipe> updateRecipe(Recipe recipe) {
        return repository.save(recipe);
    }

    @Override
    public Mono<Recipe> getRecipeById(String recipeId) {
        return repository.findById(recipeId)
                .switchIfEmpty(Mono.error(new RecipeNotFoundException("Recipe not found by id: " + recipeId)));
    }

    @Override
    public Flux<Recipe> getRecipes(int page, int size) {
        return repository.aggregateRecipes(PageRequest.of(page, size, Sort.by(Sort.Direction.DESC, "date")))
                .switchIfEmpty(Flux.error(new RecipeNotFoundException()));
    }

    @Override
    public Flux<Recipe> getRecipesByUserId(String userId, int page, int size) {
        return repository.retrieveByUserIdPaged(userId, PageRequest.of(page, size, Sort.by(Sort.Direction.DESC, "date")))
                .switchIfEmpty(Flux.error(new RecipeNotFoundException()));
    }

    @Override
    public Flux<Recipe> getRecipesByUserEmail(String userEmail, int page, int size) {
        return repository.retrieveByUserEmailPaged(userEmail, PageRequest.of(page, size, Sort.by(Sort.Direction.DESC, "date")));
    }

    @Override
    public Mono<Void> updateRecipeImages(String recipeId, ImageUrls urls) {
        return repository.updateFields(recipeId,
                new String[]{"imageUrls.large", "imageUrls.medium", "imageUrls.small"},
                new Object[]{urls.getLarge(), urls.getMedium(), urls.getSmall()});
    }

    @Override
    public Mono<Void> updateRecipeStepImage(String stepId, String url) {
        return repository.updateRecipeStepImageUrl(stepId, url);
    }

    @Override
    public Mono<Void> updateRecipeAdImage(String adId, String url) {
        return repository.updateRecipeAdImageUrl(adId, url);
    }

    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    @Override
    public Mono<Void> activateRecipe(String recipeId) {
        return repository.updateFields(recipeId, new String[]{"enable"}, new Object[]{true});
    }

    @Override
    public Mono<Void> updateRecipeStepsRecipeId(String stepId, String recipeId) {
        return repository.updateRecipeStepsRecipeId(stepId, recipeId);
    }

    @Override
    public Mono<Void> updateRecipeAdRecipeId(String adId, String recipeId) {
        return repository.updateRecipeAdRecipeId(adId, recipeId);
    }

    @Override
    public Flux<Recipe> getRecipesByDrinkType(DrinkType drinkType, int page, int size) {
        return repository.retrieveByDrinkTypePaged(drinkType, PageRequest.of(page, size, Sort.by(Sort.Direction.DESC, "date")))
                .switchIfEmpty(Flux.error(new RecipeNotFoundException()));
    }

    @Override
    public Flux<Recipe> searchRecipe(RecipesToSearch recipesToSearch, int page, int size) {
        return repository.searchRecipe(recipesToSearch, PageRequest.of(page, size))
                .switchIfEmpty(Flux.error(new RecipeNotFoundException()));
    }

    @Override
    public Mono<Void> incRecipeViewed(String recipeId) {
        return repository.incRecipeViewed(recipeId);
    }

    @Override
    public Mono<Void> incRecipeAdClicked(String adId) {
        return repository.incRecipeAdClicked(adId);
    }

    @Override
    public Mono<Void> incRecipeLiked(String recipeId) {
        return repository.incRecipeLiked(recipeId);
    }

    @Override
    public Mono<Void> decRecipeLiked(String recipeId) {
        return repository.decRecipeLiked(recipeId);
    }

    @Override
    public Flux<Recipe> getAllUnusedRecipes() {
        return repository.findAllByEnable(false)
                .filter(r -> r.getDate().after(new Date(System.currentTimeMillis() + (30 * 60 * 1000))));
    }
}
