package ru.nudlik.service.es;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.nudlik.domain.es.SearchExample;
import ru.nudlik.es.repository.SearchExampleRepository;
import ru.nudlik.repository.RecipeRepository;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class SearchExampleServiceImpl implements SearchExampleService {
    @Autowired
    private SearchExampleRepository repository;
    @Autowired
    private RecipeRepository recipeRepository;

    @Override
    public Mono<Void> createSearchExample(String recipeId) {
        return recipeRepository.findById(recipeId)
                .map(recipe -> {
                    ArrayList<String> list = new ArrayList<>();
                    list.add(recipe.getName().toLowerCase());
                    if (recipe.getSecondaryName() != null && recipe.getSecondaryName().length() > 0) {
                        list.add(recipe.getSecondaryName().toLowerCase());
                    }
                    Arrays.stream(recipe.getSearchIndex().split(",")).forEach(s -> list.add(s.trim().toLowerCase()));
                    return list;
                })
                .flatMapIterable(list -> list)
                .flatMap(item -> {
                    SearchExample example = new SearchExample();
                    example.setExampleId((long)(recipeId.hashCode() + item.hashCode()));
                    example.setExample(item);
                    return repository.save(example);
                })
                .then();
    }

    @Override
    public Mono<Void> deleteSearchExample(String recipeId) {
        return recipeRepository.findById(recipeId)
                .map(recipe -> {
                    ArrayList<String> list = new ArrayList<>();
                    list.add(recipe.getName().toLowerCase());
                    if (recipe.getSecondaryName() != null && recipe.getSecondaryName().length() > 0) {
                        list.add(recipe.getSecondaryName().toLowerCase());
                    }
                    Arrays.stream(recipe.getSearchIndex().split(",")).forEach(s -> list.add(s.trim().toLowerCase()));
                    return list;
                })
                .flatMapIterable(list -> list)
                .flatMap(item -> {
                    long id = recipeId.hashCode() + item.hashCode();
                    return repository.deleteById(id);
                })
                .then();
    }

    @Override
    public Flux<SearchExample> searchExamples(String value, int count) {
        if (value.isEmpty()) {
            return repository.findByExampleLike(value, PageRequest.of(0, count));
        }
        return repository.findExample(value, PageRequest.of(0, count));
    }
}
