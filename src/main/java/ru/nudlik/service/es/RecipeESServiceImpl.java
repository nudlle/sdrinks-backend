package ru.nudlik.service.es;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.nudlik.domain.DrinkState;
import ru.nudlik.domain.DrinkTaste;
import ru.nudlik.domain.es.RecipeES;
import ru.nudlik.domain.es.RecipeESRequest;
import ru.nudlik.repository.RecipeRepository;
import ru.nudlik.es.repository.RecipeESRepository;

import java.util.List;

@Service
public class RecipeESServiceImpl implements RecipeESService {
    @Autowired
    private RecipeESRepository repository;
    @Autowired
    private RecipeRepository recipeRepository;

    @Override
    public Mono<RecipeES> createRecipeIndex(String recipeId) {
        return recipeRepository.findById(recipeId)
                .flatMap(recipe -> {
                    RecipeES es = new RecipeES();
                    es.setRecipeESId((long) recipe.getRecipeId().hashCode());
                    es.setRecipeId(recipe.getRecipeId());
                    es.setUserId(recipe.getUserId());
                    es.setUserEmail(recipe.getUserEmail());
                    es.setName(recipe.getName());
                    es.setSecondaryName(recipe.getSecondaryName());
                    es.setDrinkState(recipe.getDrinkState());
                    es.setDrinkType(recipe.getDrinkType());
                    es.setDrinkTastes(recipe.getDrinkTastes());
                    es.setDrinkAlcohol(recipe.getDrinkAlcohol());
                    es.setHashTags(recipe.getSearchIndex());
                    es.setImageUrls(recipe.getImageUrls());
                    return repository.save(es);
                });
    }

    @Override
    public Mono<Void> deleteRecipeES(String recipeId) {
        return repository.deleteById((long) recipeId.hashCode());
    }

    @Override
    public Flux<RecipeES> findRecipeES(String query, RecipeESRequest re, int page, int size) {
        return repository.findRecipeES(query, re, PageRequest.of(page, size));
    }

    private Flux<RecipeES> checkEmpty(RecipeESRequest re, int page, int size) {
        if (re.getDrinkState() == null &&
                re.getDrinkType() == null &&
                re.getDrinkAlcohol() == null &&
                (re.getDrinkTastes() == null || re.getDrinkTastes().size() == 0)) {
            return Flux.empty();
        }
        return repository.findByNameLike("", PageRequest.of(page, size));
    }
}
