package ru.nudlik.service.es;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.nudlik.domain.es.RecipeES;
import ru.nudlik.domain.es.RecipeESRequest;

public interface RecipeESService {
    Mono<RecipeES> createRecipeIndex(String recipeId);
    Mono<Void> deleteRecipeES(String recipeId);
    Flux<RecipeES> findRecipeES(String query, RecipeESRequest recipeESRequest, int page, int size);
}
