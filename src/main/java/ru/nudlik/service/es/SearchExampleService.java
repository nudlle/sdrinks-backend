package ru.nudlik.service.es;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.nudlik.domain.es.SearchExample;

public interface SearchExampleService {
    Mono<Void> createSearchExample(String recipeId);
    Mono<Void> deleteSearchExample(String recipeId);
    Flux<SearchExample> searchExamples(String value, int count);
}
