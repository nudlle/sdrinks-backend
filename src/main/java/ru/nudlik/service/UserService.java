package ru.nudlik.service;

import reactor.core.publisher.Mono;
import ru.nudlik.domain.Subscription;
import ru.nudlik.domain.User;

public interface UserService {
    Mono<User> createUser(User user);
    Mono<Void> deleteUser(String userId);
    Mono<User> getUser(String userId);
    Mono<Void> updateUserPassword(String userId, String newPassword);
    Mono<Void> updateUserAvatar(String userId, String avatarPath);
    Mono<Void> putUserToBlacklist(String userId, boolean addOrDelete);
    Mono<User> getUserByEmail(String email);
    Mono<Void> putLikedRecipe(String userId, String recipeId);
    Mono<Void> deleteLikedRecipe(String userId, String recipeId);
    Mono<Void> putOwnRecipe(String userId, String recipeId);
    Mono<Void> deleteOwnRecipe(String userId, String recipeId);
    Mono<Void> putSubscription(Subscription subscription);
    Mono<Void> activateSubscription(String id);
}
