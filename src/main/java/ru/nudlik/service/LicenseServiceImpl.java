package ru.nudlik.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;
import ru.nudlik.exceptions.ResourceLoadException;
import ru.nudlik.service.storage.FileStorageService;

@Service
public class LicenseServiceImpl implements LicenseService {
    @Autowired
    private FileStorageService fileStorageService;
    @Override
    public Mono<String> getLicense() {
        return fileStorageService.loadLicense()
                .switchIfEmpty(Mono.error(new ResourceLoadException()));
    }
}
