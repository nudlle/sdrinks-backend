package ru.nudlik.es.repository;

import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.repository.ReactiveElasticsearchRepository;
import reactor.core.publisher.Flux;
import ru.nudlik.domain.es.SearchExample;


public interface SearchExampleRepository extends ReactiveElasticsearchRepository<SearchExample, Long>, SearchExampleRepositoryCustom{
    Flux<SearchExample> findByExampleLike(String query, Pageable page);
}
