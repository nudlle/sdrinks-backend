package ru.nudlik.es.repository;

import org.springframework.data.domain.Pageable;
import reactor.core.publisher.Flux;
import ru.nudlik.domain.es.RecipeES;
import ru.nudlik.domain.es.RecipeESRequest;

public interface RecipeESRepositoryCustom {
    Flux<RecipeES> findRecipeES(String queryText, RecipeESRequest recipeESRequest, Pageable page);
}
