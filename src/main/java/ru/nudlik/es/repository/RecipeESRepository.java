package ru.nudlik.es.repository;


import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.repository.ReactiveElasticsearchRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import ru.nudlik.domain.es.RecipeES;

@Repository
public interface RecipeESRepository extends ReactiveElasticsearchRepository<RecipeES, Long>, RecipeESRepositoryCustom {
    Flux<RecipeES> findByNameLike(String name, Pageable page);
    Flux<RecipeES> findBySecondaryNameLike(String secondaryName, Pageable page);
    Flux<RecipeES> findByUserEmailLike(String userEmail, Pageable page);
    Flux<RecipeES> findByHashTagsLike(String hashTagsLike, Pageable page);
    Flux<RecipeES> findByNameOrSecondaryNameOrUserEmailOrHashTags(String name,
                                                                        String sName,
                                                                        String email,
                                                                        String hashTags,
                                                                        Pageable page);
}
