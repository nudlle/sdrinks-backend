package ru.nudlik.es.repository;

import org.elasticsearch.common.unit.Fuzziness;
import org.elasticsearch.index.query.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.ReactiveElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import reactor.core.publisher.Flux;
import ru.nudlik.domain.DrinkAlcohol;
import ru.nudlik.domain.DrinkState;
import ru.nudlik.domain.DrinkType;
import ru.nudlik.domain.es.RecipeES;
import ru.nudlik.domain.es.RecipeESRequest;

public class RecipeESRepositoryCustomImpl implements RecipeESRepositoryCustom {
    @Autowired
    private ReactiveElasticsearchTemplate template;

    @Override
    public Flux<RecipeES> findRecipeES(String queryText, RecipeESRequest recipeESRequest, Pageable page) {
        NativeSearchQueryBuilder builder = new NativeSearchQueryBuilder();

        BoolQueryBuilder q = QueryBuilders.boolQuery();
        QueryBuilder multiMatchQuery = QueryBuilders.multiMatchQuery(queryText,
                "name", "secondaryName", "hashTags").operator(Operator.OR)
                .fuzziness(Fuzziness.TWO)
                .prefixLength(3)
                .type(MultiMatchQueryBuilder.Type.BEST_FIELDS);
        q.should(multiMatchQuery);
        QueryBuilder matchPhrasePrefix = QueryBuilders.matchPhrasePrefixQuery("userEmail", queryText);
        q.should(matchPhrasePrefix);
        builder.withQuery(q);
        final BoolQueryBuilder f = QueryBuilders.boolQuery();
        if (recipeESRequest.getDrinkType() != null && recipeESRequest.getDrinkType() != DrinkType.Unspecified) {
            QueryBuilder matchTypeQuery = QueryBuilders.matchQuery("drinkType", recipeESRequest.getDrinkType());
            f.must(matchTypeQuery);
        }
        if (recipeESRequest.getDrinkState() != null && recipeESRequest.getDrinkState() != DrinkState.Unspecified) {
            QueryBuilder matchStateQuery = QueryBuilders.matchQuery("drinkState", recipeESRequest.getDrinkState());
            f.must(matchStateQuery);
        }
        if (recipeESRequest.getDrinkAlcohol() != null && recipeESRequest.getDrinkAlcohol() != DrinkAlcohol.Unspecified) {
            QueryBuilder matchDrinkAlcohol = QueryBuilders.matchQuery("drinkAlcohol", recipeESRequest.getDrinkAlcohol());
            f.must(matchDrinkAlcohol);
        }
        if (recipeESRequest.getDrinkTastes() != null && recipeESRequest.getDrinkTastes().size() > 0) {
            recipeESRequest.getDrinkTastes().forEach(t -> {
                QueryBuilder matchDrinkTaste = QueryBuilders.matchQuery("drinkTastes", t);
                f.should(matchDrinkTaste);
            });
        }

        builder.withFilter(f);
        builder.withPageable(page);

        NativeSearchQuery query = builder.build();

        return template.find(query, RecipeES.class);
    }
}
