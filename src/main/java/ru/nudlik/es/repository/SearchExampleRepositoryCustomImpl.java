package ru.nudlik.es.repository;

import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.ReactiveElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import reactor.core.publisher.Flux;
import ru.nudlik.domain.es.SearchExample;

public class SearchExampleRepositoryCustomImpl implements SearchExampleRepositoryCustom {
    @Autowired
    private ReactiveElasticsearchTemplate template;
    @Override
    public Flux<SearchExample> findExample(String query, Pageable page) {
        NativeSearchQueryBuilder nativeSearchQueryBuilder = new NativeSearchQueryBuilder();

        QueryBuilder matchPhraseQuery = QueryBuilders.matchPhrasePrefixQuery("example", query);

        nativeSearchQueryBuilder.withQuery(matchPhraseQuery);
        nativeSearchQueryBuilder.withPageable(page);

        NativeSearchQuery nativeSearchQuery = nativeSearchQueryBuilder.build();

        return template.find(nativeSearchQuery, SearchExample.class);
    }
}
