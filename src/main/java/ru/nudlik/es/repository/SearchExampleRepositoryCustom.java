package ru.nudlik.es.repository;

import org.springframework.data.domain.Pageable;
import reactor.core.publisher.Flux;
import ru.nudlik.domain.es.SearchExample;

public interface SearchExampleRepositoryCustom {
    Flux<SearchExample> findExample(String query, Pageable page);
}
