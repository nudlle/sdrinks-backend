package ru.nudlik.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import ru.nudlik.domain.es.RecipeES;
import ru.nudlik.domain.es.RecipeESRequest;
import ru.nudlik.domain.es.SearchExample;
import ru.nudlik.service.es.RecipeESService;
import ru.nudlik.service.es.SearchExampleService;

@RestController
@RequestMapping("search")
public class SearchController {
    @Autowired
    private RecipeESService recipeESService;
    @Autowired
    private SearchExampleService searchExampleService;

    @GetMapping("auto/{count}")
    public Flux<SearchExample> findSearchExample(@PathVariable("count") int s,
                                                 @RequestParam(value = "q") String query) {
        return searchExampleService.searchExamples(query, s);
    }

    @PostMapping("recipes/{page}/{size}")
    public Flux<RecipeES> findRecipeES(@RequestParam(value = "q") String query,
                                       @RequestBody RecipeESRequest recipeESRequest,
                                       @PathVariable("page") int page,
                                       @PathVariable("size") int size) {
        return recipeESService.findRecipeES(query, recipeESRequest, page, size);
    }
}
