package ru.nudlik.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;
import ru.nudlik.exceptions.ErrorResponseFactory;
import ru.nudlik.response.ServerResponseCode;
import ru.nudlik.response.UserResponse;
import ru.nudlik.service.RecipeService;
import ru.nudlik.service.UserService;

@RestController
@RequestMapping("users")
public class UserController {
    @Autowired
    private RecipeService recipeService;
    @Autowired
    private UserService userService;

    @PreAuthorize("hasRole('ADMIN') or hasRole('USER')")
    @PostMapping("liked-recipe/{userId}/{recipeId}")
    public Mono<ServerResponseCode> addLikedRecipe(@PathVariable("userId") String userId,
                                                    @PathVariable("recipeId") String recipeId) {
        return userService.putLikedRecipe(userId, recipeId)
                .then(recipeService.incRecipeLiked(recipeId))
                .thenReturn(ServerResponseCode.OK_RESPONSE)
                .onErrorResume(ErrorResponseFactory::convertToServerResponse);
    }

    @PreAuthorize("hasRole('ADMIN') or hasRole('USER')")
    @DeleteMapping("liked-recipe/{userId}/{recipeId}")
    public Mono<ServerResponseCode> deleteLikedRecipe(@PathVariable("userId") String userId,
                                                      @PathVariable("recipeId") String recipeId) {
        return userService.deleteLikedRecipe(userId, recipeId)
                .then(recipeService.decRecipeLiked(recipeId))
                .thenReturn(ServerResponseCode.OK_RESPONSE)
                .onErrorResume(ErrorResponseFactory::convertToServerResponse);
    }

    @PreAuthorize("hasRole('ADMIN') or hasRole('USER')")
    @GetMapping("user/{userId}")
    public Mono<UserResponse> getUser(@PathVariable("userId") String userId) {
        return userService.getUser(userId)
                .map(UserResponse::new)
                .onErrorResume(ErrorResponseFactory::convertUserResponse);
    }
}
