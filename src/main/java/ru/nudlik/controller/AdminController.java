package ru.nudlik.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;
import ru.nudlik.domain.Recipe;
import ru.nudlik.exceptions.ErrorResponseFactory;
import ru.nudlik.response.RecipeResponse;
import ru.nudlik.response.ServerResponseCode;
import ru.nudlik.service.CommentService;
import ru.nudlik.service.RecipeService;
import ru.nudlik.service.UserService;
import ru.nudlik.service.storage.FileStorageService;


@RestController
@RequestMapping("/admin")
public class AdminController {
    @Autowired
    private UserService userService;

    @Autowired
    private CommentService commentService;

    @Autowired
    private RecipeService recipeService;

    @Autowired
    private FileStorageService storageService;

    @PutMapping("/put-user-to-blacklist/{userId}/{bals}")
    public Mono<ServerResponseCode> putUserToBlacklist(@PathVariable("userId") String userId,
                                                       @PathVariable("bals") String bals) {
        return userService.putUserToBlacklist(userId, Boolean.parseBoolean(bals))
                .thenReturn(ServerResponseCode.OK_RESPONSE)
                .onErrorResume(ErrorResponseFactory::convertToServerResponse);
    }

    @DeleteMapping("/delete-user/{userId}")
    public Mono<ServerResponseCode> deleteUser(@PathVariable("userId") String userId) {
        return userService.deleteUser(userId)
                .doOnNext(v -> storageService.deleteUserDir(userId).subscribe())
                .thenReturn(ServerResponseCode.OK_RESPONSE)
                .onErrorResume(ErrorResponseFactory::convertToServerResponse);
    }

    @DeleteMapping("/delete-recipe/{recipeId}")
    public Mono<ServerResponseCode> deleteRecipe(@PathVariable("recipeId") String recipeId) {
        return recipeService.deleteRecipe(recipeId)
                .doOnNext(v -> storageService.deleteRecipeDir(recipeId).log().subscribe())
                .thenReturn(ServerResponseCode.OK_RESPONSE)
                .onErrorResume(ErrorResponseFactory::convertToServerResponse);
    }

    @PutMapping("/update-recipe")
    public Mono<RecipeResponse> updateRecipe(@RequestBody Recipe recipe) {
        return recipeService.updateRecipe(recipe)
                .map(RecipeResponse::new)
                .onErrorResume(ErrorResponseFactory::convertRecipeResponse);
    }

    @DeleteMapping("/delete-comment/{commentId}")
    public Mono<ServerResponseCode> deleteComment(@PathVariable("commentId") String commentId) {
        return commentService.deleteComment(commentId)
                .thenReturn(ServerResponseCode.OK_RESPONSE)
                .onErrorResume(ErrorResponseFactory::convertToServerResponse);
    }
}
