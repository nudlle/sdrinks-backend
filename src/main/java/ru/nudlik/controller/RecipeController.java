package ru.nudlik.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.nudlik.domain.DrinkType;
import ru.nudlik.domain.Recipe;
import ru.nudlik.domain.RecipesToSearch;
import ru.nudlik.domain.SubValidation;
import ru.nudlik.exceptions.DeleteRestricted;
import ru.nudlik.exceptions.ErrorResponseFactory;
import ru.nudlik.exceptions.SubscriptionNotValidException;
import ru.nudlik.response.RecipeResponse;
import ru.nudlik.response.ServerResponseCode;
import ru.nudlik.service.RecipeService;
import ru.nudlik.service.SubscriptionService;
import ru.nudlik.service.UserService;
import ru.nudlik.service.es.RecipeESService;
import ru.nudlik.service.es.SearchExampleService;
import ru.nudlik.service.storage.FileStorageService;

@RestController
@RequestMapping("recipes")
public class RecipeController {
    @Autowired
    private RecipeService recipeService;
    @Autowired
    private UserService userService;
    @Autowired
    private FileStorageService fileStorageService;
    @Autowired
    private SubscriptionService subscriptionService;
    @Autowired
    private RecipeESService recipeESService;
    @Autowired
    private SearchExampleService searchExampleService;

    private Mono<SubValidation> checkRecipeAd(Recipe recipe) {
        if (recipe.getAd() != null) {
            return subscriptionService.checkSubscription(recipe.getUserId());
        } else {
            return Mono.just(SubValidation.NOT_SPECIFIED);
        }
    }

    @PostMapping("create")
    public Mono<RecipeResponse> createRecipe(@RequestBody Recipe recipe) {
        return checkRecipeAd(recipe).flatMap(checked -> {
            if (checked == SubValidation.NOT_SPECIFIED || checked == SubValidation.VALID) {
                return recipeService.createRecipe(recipe)
                        .doOnNext(r ->
                            r.getRecipeSteps().forEach(recipeStepCook -> {
                                recipeService.updateRecipeStepsRecipeId(recipeStepCook.getRecipeStepCookId(), r.getRecipeId()).subscribe();
                                recipeStepCook.setRecipeId(r.getRecipeId());
                            }))
                        .doOnNext(r -> {
                            if (r.getAd() != null) {
                                recipeService.updateRecipeAdRecipeId(r.getAd().getAdId(), r.getRecipeId()).subscribe();
                                r.getAd().setRecipeId(r.getRecipeId());
                            }});
            } else {
                throw new SubscriptionNotValidException();
            }
        })
        .map(RecipeResponse::new)
        .onErrorResume(ErrorResponseFactory::convertRecipeResponse);
    }

    @GetMapping("all/{page}/{size}")
    public Flux<RecipeResponse> getRecipes(@PathVariable("page") String page, @PathVariable("size") String size) {
        return recipeService.getRecipes(Integer.parseInt(page), Integer.parseInt(size))
                .map(RecipeResponse::new)
                .onErrorResume(ErrorResponseFactory::convertRecipeResponse);
    }

    @GetMapping("by-type/{type}/{page}/{size}")
    public Flux<RecipeResponse> getRecipesByType(@PathVariable("type") String type,
                                                 @PathVariable("page") String page,
                                                 @PathVariable("size") String size) {
        return recipeService.getRecipesByDrinkType(DrinkType.valueOf(type), Integer.parseInt(page), Integer.parseInt(size))
                .map(RecipeResponse::new)
                .onErrorResume(ErrorResponseFactory::convertRecipeResponse);
    }

    @GetMapping("by-user/{userId}/{page}/{size}")
    public Flux<RecipeResponse> getRecipesByUser(@PathVariable("userId") String userId,
                                                 @PathVariable("page") String page,
                                                 @PathVariable("size") String size) {
        return recipeService.getRecipesByUserId(userId, Integer.parseInt(page), Integer.parseInt(size))
                .map(RecipeResponse::new)
                .onErrorResume(ErrorResponseFactory::convertRecipeResponse);
    }

    @GetMapping("by-id/{recipeId}")
    public Mono<RecipeResponse> getRecipe(@PathVariable("recipeId") String recipeId) {
        return recipeService.getRecipeById(recipeId)
                .map(RecipeResponse::new)
                .onErrorResume(ErrorResponseFactory::convertRecipeResponse);
    }

    @GetMapping("by-user-email/{email}/{page}/{size}")
    public Flux<RecipeResponse> getRecipesByUserEmail(@PathVariable("email") String email,
                                                      @PathVariable("page") String page,
                                                      @PathVariable("size") String size) {
        return recipeService.getRecipesByUserEmail(email, Integer.parseInt(page), Integer.parseInt(size))
                .map(RecipeResponse::new)
                .onErrorResume(ErrorResponseFactory::convertRecipeResponse);
    }

    @PostMapping("activate/{recipeId}/{userId}")
    public Mono<ServerResponseCode> activateRecipe(@PathVariable("recipeId") String recipeId,
                                                   @PathVariable("userId") String userId) {
        return recipeService.activateRecipe(recipeId)
                .then(userService.putOwnRecipe(userId, recipeId))
                .then(recipeESService.createRecipeIndex(recipeId))
                .then(searchExampleService.createSearchExample(recipeId))
                .thenReturn(ServerResponseCode.OK_RESPONSE)
                .onErrorResume(ErrorResponseFactory::convertToServerResponse);
    }

    @DeleteMapping("delete/{recipeId}/{userId}")
    public Mono<ServerResponseCode> deleteRecipe(@PathVariable("recipeId") String recipeId,
                                                 @PathVariable("userId") String userId) {
        return recipeService.isUserRecipe(userId, recipeId)
                .flatMap(userRecipe -> {
                    if (userRecipe) {
                        return searchExampleService.deleteSearchExample(recipeId).then(recipeService.deleteRecipe(recipeId));
                    }
                    throw new DeleteRestricted();
                })
                .then(fileStorageService.deleteRecipeDir(recipeId))
                .then(userService.deleteOwnRecipe(userId, recipeId))
                .then(recipeESService.deleteRecipeES(recipeId))
                .thenReturn(ServerResponseCode.OK_RESPONSE)
                .onErrorResume(ErrorResponseFactory::convertToServerResponse);
    }

    @PutMapping("se/{page}/{size}")
    public Flux<RecipeResponse> searchRecipes(@RequestBody RecipesToSearch search,
                                              @PathVariable("page") String page,
                                              @PathVariable("size") String size) {
        return recipeService.searchRecipe(search, Integer.parseInt(page), Integer.parseInt(size))
                .map(RecipeResponse::new)
                .onErrorResume(ErrorResponseFactory::convertRecipeResponse);
    }

    @PutMapping("inc/view/{recipeId}")
    public Mono<ServerResponseCode> incRecipeView(@PathVariable("recipeId") String recipeId) {
        return recipeService.incRecipeViewed(recipeId)
                .thenReturn(ServerResponseCode.OK_RESPONSE)
                .onErrorResume(ErrorResponseFactory::convertToServerResponse);

    }

    @PutMapping("inc/click/{adId}")
    public Mono<ServerResponseCode> incRecipeAdClicked(@PathVariable("adId") String adId) {
        return recipeService.incRecipeAdClicked(adId)
                .thenReturn(ServerResponseCode.OK_RESPONSE)
                .onErrorResume(ErrorResponseFactory::convertToServerResponse);
    }
}
