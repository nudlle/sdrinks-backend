package ru.nudlik.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.nudlik.domain.UserMessage;
import ru.nudlik.exceptions.ErrorResponseFactory;
import ru.nudlik.response.UserMessageResponse;
import ru.nudlik.response.ServerResponseCode;
import ru.nudlik.service.UserMessageService;

import java.util.Date;

@RestController
@RequestMapping("user-messages")
public class UserMessageController {
    @Autowired
    private UserMessageService userMessageService;

    @PostMapping("/create")
    public Mono<UserMessageResponse> createUserMessage(@RequestBody UserMessage userMessage) {
        return userMessageService.createUserMessage(userMessage)
                .map(UserMessageResponse::new)
                .onErrorResume(ErrorResponseFactory::convertToAppErrorResponse);
    }

    @GetMapping("/by-id/{id}")
    public Mono<ServerResponseCode> getUserMessageById(@PathVariable("id") String id) {
        return userMessageService.getUserMessageById(id)
                .thenReturn(ServerResponseCode.OK_RESPONSE)
                .onErrorResume(ErrorResponseFactory::convertToServerResponse);
    }

    @DeleteMapping("/delete/{id}")
    public Mono<ServerResponseCode> deleteAppErrorRequest(@PathVariable("id") String id) {
        return userMessageService.deleteUserMessage(id)
                .thenReturn(ServerResponseCode.OK_RESPONSE)
                .onErrorResume(ErrorResponseFactory::convertToServerResponse);
    }

    @GetMapping("by-date/{date}")
    public Flux<UserMessageResponse> getAllByDate(@RequestParam("date") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
                                                               Date date) {
        return userMessageService.getUserMessageByDate(date)
                .map(UserMessageResponse::new)
                .onErrorResume(ErrorResponseFactory::convertToAppErrorResponse);
    }

    @GetMapping("by-version/{version}")
    public Flux<UserMessageResponse> getAllFixed(@RequestParam("version") String version) {
        return userMessageService.getUserMessageByAppVersion(version)
                .map(UserMessageResponse::new)
                .onErrorResume(ErrorResponseFactory::convertToAppErrorResponse);
    }

    @GetMapping("by-user/{id}")
    public Flux<UserMessageResponse> getAllByUserId(@RequestParam("id") String id) {
        return userMessageService.getUserMessageByUser(id)
                .map(UserMessageResponse::new)
                .onErrorResume(ErrorResponseFactory::convertToAppErrorResponse);
    }
}
