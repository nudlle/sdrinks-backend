package ru.nudlik.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;
import ru.nudlik.domain.User;
import ru.nudlik.exceptions.CheckEmailHashException;
import ru.nudlik.exceptions.ErrorResponseFactory;
import ru.nudlik.exceptions.LoginFailedException;
import ru.nudlik.response.ServerResponseCode;
import ru.nudlik.response.SessionTokenResponse;
import ru.nudlik.response.UserResponse;
import ru.nudlik.security.CustomUserDetailService;
import ru.nudlik.security.SessionToken;
import ru.nudlik.security.UserRequest;
import ru.nudlik.service.email.ConfirmEmailService;
import ru.nudlik.service.UserService;

@RestController
@RequestMapping("/auth")
public class AuthController {

    @Autowired
    private UserService userService;
    @Autowired
    private ConfirmEmailService emailService;
    @Autowired
    private CustomUserDetailService userDetailService;

    @PostMapping("/login")
    public Mono<SessionTokenResponse> loginAction(@RequestBody UserRequest user) {
        return userService.getUser(user.getUser().getUserId())
                .flatMap(usr -> {
                    PasswordEncoder encoder = PasswordEncoderFactories.createDelegatingPasswordEncoder();
                    if (!encoder.matches(user.getUser().getPassword(), usr.getPassword())) {
                        throw new LoginFailedException();
                    }
                    SessionToken ss = new SessionToken(usr.getUserId());
                    return userDetailService.addUserDetails(ss, "USER");
                })
                .map(SessionTokenResponse::new)
                .onErrorResume(ErrorResponseFactory::convertSessionTokenResponse);
    }

    @PostMapping("/register/{email}/{emailHash}")
    public Mono<UserResponse> registerAction(@PathVariable("email") String email,
                                             @PathVariable("emailHash") String emailHash,
                                             @RequestBody UserRequest userRequest) {
        return emailService.checkEmail(email, emailHash)
                .flatMap(result -> {
                    if (result) {
                        User user = new User(userRequest.getUser());
                        PasswordEncoder encoder = PasswordEncoderFactories.createDelegatingPasswordEncoder();
                        user.setPassword(encoder.encode(user.getPassword()));
                        return userService.createUser(user);
                    }
                    throw new CheckEmailHashException("Check email " + email + " failed");
                })
                .map(UserResponse::new)
                .onErrorResume(ErrorResponseFactory::convertUserResponse);

    }

    @GetMapping("/check-email/{email}")
    public Mono<ServerResponseCode> checkMail(@PathVariable("email") String email) {
        return userService.getUserByEmail(email)
                .map(user -> ServerResponseCode.UNIQUE_EMAIL_EXCEPTION)
                .onErrorResume(e -> {
                    emailService.generateRandomString(email).subscribe();
                    return Mono.just(ServerResponseCode.OK_RESPONSE);
                });
    }

    @PostMapping("/recover")
    public Mono<UserResponse> recoverByPassword(@RequestBody UserRequest userRequest) {
        return userService.getUserByEmail(userRequest.getUser().getEmail())
                .doOnNext(usr -> {
                    PasswordEncoder encoder = PasswordEncoderFactories.createDelegatingPasswordEncoder();
                    if (!encoder.matches(userRequest.getUser().getPassword(), usr.getPassword())) {
                        throw new LoginFailedException();
                    }
                })
                .map(UserResponse::new)
                .onErrorResume(ErrorResponseFactory::convertUserResponse);
    }

    @GetMapping("/recover-request/{email}")
    public Mono<ServerResponseCode> recoverEmailHashRequest(@PathVariable("email") String email) {
        return userService.getUserByEmail(email)
                .flatMap(user -> emailService.generateRandomString(user.getEmail()))
                .onErrorResume(ErrorResponseFactory::convertToServerResponse);
    }

    @PostMapping("/recover/{hashEmail}")
    public Mono<UserResponse> recoverByEmail(@PathVariable("hashEmail") String hashEmail,
                                             @RequestBody UserRequest userRequest) {
        return emailService.checkEmail(userRequest.getUser().getEmail(), hashEmail)
                .flatMap(result -> {
                    if (result) {
                        PasswordEncoder encoder = PasswordEncoderFactories.createDelegatingPasswordEncoder();
                        return userService.getUserByEmail(userRequest.getUser().getEmail())
                                .flatMap(user -> userService
                                        .updateUserPassword(user.getUserId(),
                                                encoder.encode(userRequest.getUser().getPassword())))
                        .thenReturn(userService.getUserByEmail(userRequest.getUser().getEmail()));
                    }
                    throw new CheckEmailHashException("Check email " + userRequest.getUser().getEmail() + " failed");
                })
                .flatMap(m -> m)
                .map(UserResponse::new)
                .onErrorResume(ErrorResponseFactory::convertUserResponse);
    }

    @PostMapping("/logout/{userId}")
    public Mono<ServerResponseCode> userLogout(@PathVariable("userId") String userId) {
        return userDetailService.deleteUserDetails(userId)
                .thenReturn(ServerResponseCode.OK_RESPONSE)
                .onErrorResume(ErrorResponseFactory::convertToServerResponse);
    }
}
