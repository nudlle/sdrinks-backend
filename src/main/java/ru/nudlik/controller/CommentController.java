package ru.nudlik.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.nudlik.domain.UserComment;
import ru.nudlik.exceptions.ErrorResponseFactory;
import ru.nudlik.response.CommentResponse;
import ru.nudlik.response.ServerResponseCode;
import ru.nudlik.service.CommentService;
import ru.nudlik.service.RecipeService;

import java.util.Date;

@RestController
@RequestMapping("/comments")
public class CommentController {
    @Autowired
    private CommentService commentService;
    @Autowired
    private RecipeService recipeService;

    @PostMapping("/create")
    public Mono<CommentResponse> postComment(@RequestBody UserComment userComment) {
        return commentService.createComment(userComment)
                .map(CommentResponse::new)
                .onErrorResume(ErrorResponseFactory::convertCommentResponse);
    }

    @GetMapping("{recipeId}/{page}/{size}")
    public Flux<CommentResponse> getCommentsOfRecipe(@PathVariable("recipeId") String recipeId,
                                                     @PathVariable("page") String page,
                                                     @PathVariable("size") String size) {
        return commentService.getComments(recipeId, Integer.parseInt(page), Integer.parseInt(size))
                .map(CommentResponse::new)
                .onErrorResume(ErrorResponseFactory::convertCommentResponse);
    }

    @GetMapping("check/{recipeId}/{userId}")
    public Mono<ServerResponseCode> isCommentPostedByUser(@PathVariable("recipeId") String recipeId,
                                                          @PathVariable("userId") String userId) {
        return commentService.isAlreadyPosted(recipeId, userId)
                .onErrorResume(ErrorResponseFactory::convertToServerResponse);
    }
}
