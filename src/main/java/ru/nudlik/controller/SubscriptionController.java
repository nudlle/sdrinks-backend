package ru.nudlik.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.nudlik.domain.Subscription;
import ru.nudlik.exceptions.ErrorResponseFactory;
import ru.nudlik.response.ServerResponseCode;
import ru.nudlik.response.SubscriptionResponse;
import ru.nudlik.service.SubscriptionService;
import ru.nudlik.service.UserService;

@RestController
@RequestMapping("subscriptions")
public class SubscriptionController {
    @Autowired
    private SubscriptionService subscriptionService;
    @Autowired
    private UserService userService;

    @PostMapping("/create-sub")
    public Mono<SubscriptionResponse> createSubscription(@RequestBody Subscription subscription) {
        return subscriptionService.createSubscription(subscription)
                .doOnNext(s -> userService.putSubscription(s).subscribe())
                .map(SubscriptionResponse::new)
                .onErrorResume(ErrorResponseFactory::convertToSubscriptionResponse);
    }

    @GetMapping("/find-subs/{userId}")
    public Flux<SubscriptionResponse> findByUserId(@PathVariable("userId") String userId) {
        return subscriptionService.getSubscriptions(userId)
                .map(SubscriptionResponse::new)
                .onErrorResume(ErrorResponseFactory::convertToSubscriptionResponse);
    }

    @DeleteMapping("/delete-sub/{subId}")
    public Mono<ServerResponseCode> deleteSubscription(@PathVariable("subId") String subId) {
        return subscriptionService.deleteSubscription(subId)
                .thenReturn(ServerResponseCode.OK_RESPONSE)
                .onErrorResume(ErrorResponseFactory::convertToServerResponse);
    }

    @DeleteMapping("/delete-all/{userId}")
    public Mono<ServerResponseCode> deleteAll(@PathVariable("userId") String userId) {
        return subscriptionService.deleteAllByUserId(userId)
                .thenReturn(ServerResponseCode.OK_RESPONSE)
                .onErrorResume(ErrorResponseFactory::convertToServerResponse);
    }

    @PutMapping("/activate-sub/{subId}")
    public Mono<ServerResponseCode> activateSub(@PathVariable("subId") String subId) {
        return subscriptionService.activateSubscription(subId)
                .doOnNext(s -> userService.activateSubscription(subId).subscribe())
                .thenReturn(ServerResponseCode.OK_RESPONSE)
                .onErrorResume(ErrorResponseFactory::convertToServerResponse);
    }

    @GetMapping("/valid-subs/{userId}")
    public Flux<SubscriptionResponse> getValidSubscriptions(@PathVariable("userId") String userId) {
        return subscriptionService.getValidSubscriptions(userId)
                .map(SubscriptionResponse::new)
                .onErrorResume(ErrorResponseFactory::convertToSubscriptionResponse);

    }
}
