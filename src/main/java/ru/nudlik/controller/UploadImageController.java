package ru.nudlik.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.codec.multipart.FilePart;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;
import ru.nudlik.domain.ImageUrls;
import ru.nudlik.exceptions.ErrorResponseFactory;
import ru.nudlik.response.ImageUrlsResponse;
import ru.nudlik.response.ServerResponseCode;
import ru.nudlik.response.StringResponse;
import ru.nudlik.service.RecipeService;
import ru.nudlik.service.UserService;
import ru.nudlik.service.storage.FileStorageService;
import ru.nudlik.service.storage.ImageResizeService;

import java.io.File;
import java.util.Map;

@RestController
@RequestMapping("/uploads")
public class UploadImageController {
    @Autowired
    private FileStorageService storageService;
    @Autowired
    private ImageResizeService imageResizeService;
    @Autowired
    private UserService userService;
    @Autowired
    private RecipeService recipeService;

    @PostMapping("/recipe-image/{recipeId}")
    public Mono<ImageUrlsResponse> uploadAndRenderRecipeImage(@PathVariable("recipeId") String recipeId,
                                                              @RequestPart("file") Mono<FilePart> file,
                                                              @RequestHeader Map<String, String> headers) {
        if (!headers.containsKey("Content-Length") || Double.parseDouble(headers.get("Content-Length")) > 5e+7) {
            return Mono.just(new ImageUrlsResponse(ServerResponseCode.FILE_IS_TOO_BIG));
        } else {
            return storageService.storeRecipeImage(recipeId, file)
                    .map(originalPath -> {
                        ImageUrls result = new ImageUrls();
//                        String large = imageResizeService.resize(originalPath, .8);
//                        String medium = imageResizeService.resize(originalPath, .6);
//                        String small = imageResizeService.resize(originalPath, .4);
                        String large = imageResizeService.resize(originalPath, 800, 600);
                        String medium = imageResizeService.resize(originalPath, 600, 400);
                        String small = imageResizeService.resize(originalPath, 400, 250);
                        result.setLarge(large.substring(large.lastIndexOf(File.separator) + 1));
                        result.setMedium(medium.substring(medium.lastIndexOf(File.separator) + 1));
                        result.setSmall(small.substring(small.lastIndexOf(File.separator) + 1));
                        storageService.deleteFile(originalPath).subscribe();
                        recipeService.updateRecipeImages(recipeId, result).subscribe();
                        return result;
                    })
                    .map(ImageUrlsResponse::new)
                    .onErrorResume(ErrorResponseFactory::convertImageUrlsResponse);
        }
    }

    @PostMapping("/recipe-step/{recipeId}/{stepId}")
    public Mono<StringResponse> uploadAndRenderRecipeStepImage(@PathVariable("recipeId") String recipeId,
                                                               @PathVariable("stepId") String stepId,
                                                               @RequestPart("file") Mono<FilePart> file,
                                                               @RequestHeader Map<String, String> headers) {
        if (!headers.containsKey("Content-Length") || Double.parseDouble(headers.get("Content-Length")) > 5e+7) {
            return Mono.just(new StringResponse(ServerResponseCode.FILE_IS_TOO_BIG));
        } else {
            return storageService.storeRecipeStepImage(recipeId, stepId, file)
                    .map(originalPath -> {
//                        String format = imageResizeService.resize(originalPath, .6);
                        String format = imageResizeService.resize(originalPath, 800, 600);
                        storageService.deleteFile(originalPath).subscribe();
                        String result = format.substring(format.lastIndexOf(File.separator) + 1);
                        recipeService.updateRecipeStepImage(stepId, result).subscribe();
                        return result;
                    })
                    .map(StringResponse::new)
                    .onErrorResume(ErrorResponseFactory::convertStringResponse);
        }
    }

    @PostMapping("/recipe-ad/{recipeId}/{adId}")
    public Mono<StringResponse> uploadAndRenderRecipeAd(@PathVariable("recipeId") String recipeId,
                                                        @PathVariable("adId") String adId,
                                                        @RequestPart("file") Mono<FilePart> file,
                                                        @RequestHeader Map<String, String> headers) {
        if (!headers.containsKey("Content-Length") || Double.parseDouble(headers.get("Content-Length")) > 5e+7) {
            return Mono.just(new StringResponse(ServerResponseCode.FILE_IS_TOO_BIG));
        } else {
            return storageService.storeRecipeAdImage(recipeId, adId, file)
                    .map(originalPath -> {
//                        String format = imageResizeService.resize(originalPath, .7);
                        String format = imageResizeService.resize(originalPath, 800, 600);
                        storageService.deleteFile(originalPath).subscribe();
                        String result = format.substring(format.lastIndexOf(File.separator) + 1);
                        recipeService.updateRecipeAdImage(adId, result).subscribe();
                        return result;
                    })
                    .map(StringResponse::new)
                    .onErrorResume(ErrorResponseFactory::convertStringResponse);
        }
    }

    @PostMapping("/user-avatar/{userId}")
    public Mono<StringResponse> uploadUserAvatarImage(@PathVariable("userId") String userId,
                                                      @RequestPart("file") Mono<FilePart> file,
                                                      @RequestHeader Map<String, String> headers) {
        if (!headers.containsKey("Content-Length") || Double.parseDouble(headers.get("Content-Length")) > 5e+7) {
            return Mono.just(new StringResponse(ServerResponseCode.FILE_IS_TOO_BIG));
        } else {
            return storageService.storeAvatarImage(userId, file)
                    .map(originalPath -> {
                        String format = imageResizeService.resize(originalPath, .6);
                        storageService.deleteFile(originalPath).subscribe();
                        String fileName = format.substring(format.lastIndexOf(File.separator) + 1);
                        userService.updateUserAvatar(userId, fileName).subscribe();
                        return fileName;
                    })
                    .map(StringResponse::new)
                    .onErrorResume(ErrorResponseFactory::convertStringResponse);
        }
    }

    @PreAuthorize("hasRole('ADMIN') or hasRole('USER')")
    @DeleteMapping("user-avatar/delete-avatar/{userId}/{fileName}")
    public Mono<ServerResponseCode> deleteUserAvatar(@PathVariable("userId") String userId,
                                                     @PathVariable("fileName") String fileName) {
        return storageService.deleteAvatar(userId, fileName)
                .thenReturn(ServerResponseCode.OK_RESPONSE)
                .onErrorResume(ErrorResponseFactory::convertToServerResponse);
    }

    @PreAuthorize("hasRole('ADMIN') or hasRole('USER')")
    @DeleteMapping("/recipe-image/{recipeId}/{fileName}")
    public Mono<ServerResponseCode> deleteRecipeImage(@PathVariable("recipeId") String recipeId,
                                                      @PathVariable("fileName") String fileName) {
        return storageService.deleteRecipeImage(recipeId, fileName)
                .thenReturn(ServerResponseCode.OK_RESPONSE)
                .onErrorResume(ErrorResponseFactory::convertToServerResponse);
    }

    @PreAuthorize("hasRole('ADMIN') or hasRole('USER')")
    @DeleteMapping("/recipe-step/{recipeId}/{stepId}/{fileName}")
    public Mono<ServerResponseCode> deleteRecipeStepImage(@PathVariable("recipeId") String recipeId,
                                                          @PathVariable("stepId") String stepId,
                                                          @PathVariable("fileName") String fileName) {
        return storageService.deleteRecipeStepImage(recipeId, stepId, fileName)
                .thenReturn(ServerResponseCode.OK_RESPONSE)
                .onErrorResume(ErrorResponseFactory::convertToServerResponse);
    }
}
