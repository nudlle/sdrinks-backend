package ru.nudlik.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;
import ru.nudlik.exceptions.ErrorResponseFactory;
import ru.nudlik.response.StringResponse;
import ru.nudlik.service.LicenseService;

@RestController
@RequestMapping("license")
public class LicenseController {
    @Autowired
    private LicenseService service;
    
    @GetMapping("usage-restrictions")
    public Mono<StringResponse> getLicenseUsageRestrictions() {
        return service.getLicense()
                .map(StringResponse::new)
                .onErrorResume(ErrorResponseFactory::convertStringResponse);
    }
}
