package ru.nudlik.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;
import ru.nudlik.exceptions.ResourceLoadException;
import ru.nudlik.service.storage.FileStorageService;

import java.io.File;

@RestController
@RequestMapping("resources")
public class ResourcesController {
    @Autowired
    private FileStorageService fileStorageService;
    private Logger logger = LoggerFactory.getLogger(ResourcesController.class.getName());

    @GetMapping("/recipes/{recipeId}/{fileName}")
    public Mono<ResponseEntity<Resource>> getRecipeMainImage(@PathVariable("recipeId") String recipeId,
                                                            @PathVariable("fileName") String fileName) {
        return fileStorageService.loadRecipeAsResource(recipeId + File.separator + fileName)
                .map(resource -> {
                    HttpHeaders headers = new HttpHeaders();
                    return new ResponseEntity<>(resource, headers, HttpStatus.OK);
                })
                .doOnError(e -> logger.warn(e.getMessage()))
                .onErrorResume(thr -> {
                    if (thr instanceof ResourceLoadException) {
                        return Mono.just(new ResponseEntity<>(HttpStatus.NO_CONTENT));
                    } else {
                        return Mono.just(new ResponseEntity<>(HttpStatus.BAD_REQUEST));
                    }
                });
    }

    @GetMapping("/recipes/{recipeId}/{stepId}/{fileName}")
    public Mono<ResponseEntity<Resource>> getRecipeStepImage(@PathVariable("recipeId") String recipeId,
                                       @PathVariable("stepId") String stepId,
                                       @PathVariable("fileName") String fileName) {
        return fileStorageService.loadRecipeAsResource(recipeId + File.separator + stepId + File.separator + fileName)
                .map(resource -> {
                    HttpHeaders headers = new HttpHeaders();
                    return new ResponseEntity<>(resource, headers, HttpStatus.OK);
                })
                .doOnError(e -> logger.warn(e.getMessage()))
                .onErrorResume(thr -> {
                    if (thr instanceof ResourceLoadException) {
                        return Mono.just(new ResponseEntity<>(HttpStatus.NO_CONTENT));
                    } else {
                        return Mono.just(new ResponseEntity<>(HttpStatus.BAD_REQUEST));
                    }
                });
    }

    @GetMapping("/ad/{recipeId}/{adId}/{fileName}")
    public Mono<ResponseEntity<Resource>> getRecipeAdImage(@PathVariable("recipeId") String recipeId,
                                                           @PathVariable("adId") String adId,
                                                           @PathVariable("fileName") String fileName) {
        return fileStorageService.loadRecipeAsResource(recipeId + File.separator + adId + File.separator + fileName)
                .map(resource -> {
                    HttpHeaders headers = new HttpHeaders();
                    return new ResponseEntity<>(resource, headers, HttpStatus.OK);
                })
                .doOnError(e -> logger.warn(e.getMessage()))
                .onErrorResume(thr -> {
                    if (thr instanceof ResourceLoadException) {
                        return Mono.just(new ResponseEntity<>(HttpStatus.NO_CONTENT));
                    } else {
                        return Mono.just(new ResponseEntity<>(HttpStatus.BAD_REQUEST));
                    }
                });
    }

    @GetMapping("/avatars/{userId}/{fileName}")
    public Mono<ResponseEntity<Resource>> getAvatarImage(@PathVariable("userId") String userId,
                                         @PathVariable("fileName") String fileName) {
        return fileStorageService.loadAvatarAsResource(userId + File.separator + fileName)
                .map(resource -> {
                    HttpHeaders headers = new HttpHeaders();
                    return new ResponseEntity<>(resource, headers, HttpStatus.OK);
                })
                .doOnError(e -> logger.warn(e.getMessage()))
                .onErrorResume(thr -> {
                    if (thr instanceof ResourceLoadException) {
                        return Mono.just(new ResponseEntity<>(HttpStatus.NO_CONTENT));
                    } else {
                        return Mono.just(new ResponseEntity<>(HttpStatus.BAD_REQUEST));
                    }
                });
    }
}
