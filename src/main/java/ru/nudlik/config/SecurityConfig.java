package ru.nudlik.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.security.config.annotation.method.configuration.EnableReactiveMethodSecurity;
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity;
import org.springframework.security.config.web.server.ServerHttpSecurity;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.server.SecurityWebFilterChain;
import org.springframework.security.web.server.ServerAuthenticationEntryPoint;
import org.springframework.security.web.server.authentication.HttpBasicServerAuthenticationEntryPoint;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.reactive.CorsConfigurationSource;
import org.springframework.web.cors.reactive.UrlBasedCorsConfigurationSource;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;
import ru.nudlik.security.CustomMapReactiveUserDetailsService;
import ru.nudlik.security.CustomUserDetailService;

import java.util.Arrays;
import java.util.List;

import static org.springframework.http.HttpHeaders.WWW_AUTHENTICATE;

@Configuration
@EnableWebFluxSecurity
@EnableReactiveMethodSecurity
public class SecurityConfig {
    @Bean
    public CustomUserDetailService userDetailsService() {
        PasswordEncoder encoder = PasswordEncoderFactories.createDelegatingPasswordEncoder();
        UserDetails admin = User.withUsername("admin")
                .password(encoder.encode("nimda123"))
                .roles("ADMIN").build();
        return new CustomMapReactiveUserDetailsService(admin);
    }

    @Bean
    CorsConfigurationSource corsConfigurationSource() {
        CorsConfiguration configuration = new CorsConfiguration();
        configuration.setAllowedOrigins(Arrays.asList("http://127.0.0.1:4200"));
//        configuration.setAllowedOrigins(Arrays.asList("http://192.168.88.243:4200"));
        configuration.setAllowedMethods(Arrays.asList("GET","POST", "PUT", "DELETE", "OPTIONS"));
        configuration.setAllowedHeaders(Arrays.asList("*"));
        configuration.setAllowCredentials(true);
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", configuration);
        return source;
    }

    @Bean
    public SecurityWebFilterChain springSecurityFilterChain(ServerHttpSecurity http) {
        return http
                .authorizeExchange()
                .pathMatchers("/resources/recipes/**").permitAll()
                .pathMatchers("/resources/ad/**").permitAll()
                .pathMatchers("/resources/avatars/**").permitAll()
                .pathMatchers("/auth/check-email/**").permitAll()
                .pathMatchers("/auth/register/**").permitAll()
                .pathMatchers("/auth/recover").permitAll()
                .pathMatchers("/auth/login").permitAll()
                .pathMatchers("/auth/recover").permitAll()
                .pathMatchers("/auth/recover-request/*").permitAll()
                .pathMatchers("/auth/recover/*").permitAll()
                .pathMatchers("/auth/logout/*").permitAll()
                .pathMatchers("/recipes/all/**").permitAll()
                .pathMatchers("/recipes/by-id/*").permitAll()
                .pathMatchers("/recipes/se/*/*").permitAll()
                .pathMatchers("/recipes/inc/*/*").permitAll()
                .pathMatchers("/comments/**").permitAll()
                .pathMatchers("/user-messages/create").permitAll()
                .pathMatchers("/license/usage-restrictions").permitAll()
                .pathMatchers("/search/auto/*").permitAll()
                .pathMatchers("/search/recipes/*/*").permitAll()
                .anyExchange().authenticated()
                .and()
                .httpBasic()
                .and()
                .cors()
                .and()
                .csrf().disable()
                .build();
    }

    @Bean
    public ServerAuthenticationEntryPoint authenticationEntryPoint() {
        HttpBasicServerAuthenticationEntryPoint point =
                new HttpBasicServerAuthenticationEntryPoint();
        point.setRealm("SDrinks");
        return point;
    }

    private static class NoPopupBasicAuthentication implements ServerAuthenticationEntryPoint {
        private static String WWW_AUTHENTICATE_FORMAT = "Basic realm=\"%s\"";
        private static String DEFAULT_REALM = "SDrinks";
        @Override
        public Mono<Void> commence(ServerWebExchange exchange, AuthenticationException e) {
            return Mono.fromRunnable(() -> {
                ServerHttpResponse response = exchange.getResponse();
                List<String> requestedWith = exchange.getRequest().getHeaders().get("X-Requested-With");
                if (requestedWith == null || !requestedWith.contains("XMLHttpRequest")) {
                    response.getHeaders().set(WWW_AUTHENTICATE,
                            String.format(WWW_AUTHENTICATE_FORMAT, DEFAULT_REALM));
                }
            });
        }
    }
}
