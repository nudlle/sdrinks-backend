package ru.nudlik.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.elasticsearch.client.ClientConfiguration;
import org.springframework.data.elasticsearch.client.reactive.ReactiveElasticsearchClient;
import org.springframework.data.elasticsearch.client.reactive.ReactiveRestClients;
import org.springframework.data.elasticsearch.repository.config.EnableReactiveElasticsearchRepositories;

@Configuration
@EnableReactiveElasticsearchRepositories(basePackages = "ru.nudlik.es.repository")
public class ElasticConfig {

    @Value("${SDRINKS_ES_HOST:localhost}")
    private String host;
    @Value("${SDRINKS_ES_PORT:9212}")
    private int port;

    @Bean
    public ReactiveElasticsearchClient client() {
        ClientConfiguration configuration = ClientConfiguration.builder()
                .connectedTo(host + ":" + port).build();
        return ReactiveRestClients.create(configuration);
    }

//    @Bean
//    public Client client() throws UnknownHostException {
//        Settings settings = Settings.builder()
//                .put("cluster.name", clusterName).build();
//        return new PreBuiltTransportClient(settings)
//                    .addTransportAddress(new TransportAddress(InetAddress.getByName(host), port));
//    }

//    @Bean
//    public ElasticsearchOperations elasticsearchTemplate() throws Exception {
//        return new ElasticsearchTemplate(client());
//    }
}
