package ru.nudlik.config;

import com.mongodb.ConnectionString;
import com.mongodb.reactivestreams.client.MongoClient;
import com.mongodb.reactivestreams.client.MongoClients;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.config.AbstractReactiveMongoConfiguration;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.data.mongodb.core.mapping.event.ValidatingMongoEventListener;
import org.springframework.data.mongodb.repository.config.EnableReactiveMongoRepositories;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import ru.nudlik.listener.CommentSaveMongoListener;

@Configuration
@EnableReactiveMongoRepositories(basePackages = "ru.nudlik.repository")
public class MongoReactiveConfig extends AbstractReactiveMongoConfiguration {

    @Value("${SDRINKS_MONGODB_HOST:localhost}")
    private String mongodbHost;
//    @Value("${SDRINKS_MONGODB_PORT:27017}")
    @Value("${SDRINKS_MONGODB_PORT:29142}")
    private String mongodbPort;

    @Bean
    public ValidatingMongoEventListener validatingMongoEventListener() {
        return new ValidatingMongoEventListener(validator());
    }

    @Bean
    public LocalValidatorFactoryBean validator() {
        return new LocalValidatorFactoryBean();
    }

    @Bean
    @Override
    public MongoClient reactiveMongoClient() {
        ConnectionString connectionString = new ConnectionString("mongodb://s-drinks-user:1234sfasASDFasf_asfdf124()*(@" + mongodbHost + ":" + mongodbPort + "/s-drinks");
//        ConnectionString connectionString = new ConnectionString("mongodb://s-drinks-user:nImda321Admin(874*&_-)@localhost:29142/s-drinks");

        return MongoClients.create(connectionString);
    }

    @Bean
    @Override
    public ReactiveMongoTemplate reactiveMongoTemplate() {
        return new ReactiveMongoTemplate(reactiveMongoDbFactory());
    }

    @Bean
    public CommentSaveMongoListener commentSaveMongoListener() {
        return new CommentSaveMongoListener();
    }

    @Override
    protected String getDatabaseName() {
        return "s-drinks";
    }

    @Override
    protected boolean autoIndexCreation() {
        return true;
    }
}
