package ru.nudlik.config;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import ru.nudlik.service.storage.FileStorageProperties;

@Configuration
@EnableConfigurationProperties({
        FileStorageProperties.class
})
public class AppConfig {
}
