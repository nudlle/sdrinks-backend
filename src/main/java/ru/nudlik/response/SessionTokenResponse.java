package ru.nudlik.response;

import ru.nudlik.security.SessionToken;

public class SessionTokenResponse extends DataResponse<SessionToken> {
    public SessionTokenResponse() {
    }

    public SessionTokenResponse(SessionToken data, ServerResponseCode code) {
        super(data, code);
    }

    public SessionTokenResponse(SessionToken data) {
        super(data);
    }

    public SessionTokenResponse(ServerResponseCode code) {
        super(code);
    }
}
