package ru.nudlik.response;

import ru.nudlik.domain.Recipe;

public class RecipeResponse extends DataResponse<Recipe> {
    public RecipeResponse() {
    }

    public RecipeResponse(Recipe data, ServerResponseCode code) {
        super(data, code);
    }

    public RecipeResponse(Recipe data) {
        super(data);
    }

    public RecipeResponse(ServerResponseCode code) {
        super(code);
    }
}
