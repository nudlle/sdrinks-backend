package ru.nudlik.response;

import ru.nudlik.domain.User;

public class UserResponse extends DataResponse<User> {
    public UserResponse() {
    }

    public UserResponse(User data, ServerResponseCode code) {
        super(data, code);
    }

    public UserResponse(User data) {
        super(data);
    }

    public UserResponse(ServerResponseCode code) {
        super(code);
    }
}
