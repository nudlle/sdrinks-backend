package ru.nudlik.response;

import ru.nudlik.domain.UserComment;

public class CommentResponse extends DataResponse<UserComment> {
    public CommentResponse() {
    }

    public CommentResponse(UserComment data, ServerResponseCode code) {
        super(data, code);
    }

    public CommentResponse(UserComment data) {
        super(data);
    }

    public CommentResponse(ServerResponseCode code) {
        super(code);
    }
}
