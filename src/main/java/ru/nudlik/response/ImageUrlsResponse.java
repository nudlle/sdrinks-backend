package ru.nudlik.response;

import ru.nudlik.domain.ImageUrls;

public class ImageUrlsResponse extends DataResponse<ImageUrls> {
    public ImageUrlsResponse() {
    }

    public ImageUrlsResponse(ImageUrls data, ServerResponseCode code) {
        super(data, code);
    }

    public ImageUrlsResponse(ImageUrls data) {
        super(data);
    }

    public ImageUrlsResponse(ServerResponseCode code) {
        super(code);
    }
}
