package ru.nudlik.response;

import ru.nudlik.domain.Subscription;

public class SubscriptionResponse extends DataResponse<Subscription> {
    public SubscriptionResponse() {
    }

    public SubscriptionResponse(Subscription data, ServerResponseCode code) {
        super(data, code);
    }

    public SubscriptionResponse(Subscription data) {
        super(data);
    }

    public SubscriptionResponse(ServerResponseCode code) {
        super(code);
    }
}
