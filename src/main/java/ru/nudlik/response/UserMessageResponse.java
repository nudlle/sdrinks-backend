package ru.nudlik.response;

import ru.nudlik.domain.UserMessage;

public class UserMessageResponse extends DataResponse<UserMessage> {
    public UserMessageResponse() {
    }

    public UserMessageResponse(UserMessage data, ServerResponseCode code) {
        super(data, code);
    }

    public UserMessageResponse(UserMessage data) {
        super(data);
    }

    public UserMessageResponse(ServerResponseCode code) {
        super(code);
    }
}
