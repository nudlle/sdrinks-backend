package ru.nudlik.response;

public class StringResponse extends DataResponse<String> {
    public StringResponse() {
    }

    public StringResponse(String data, ServerResponseCode code) {
        super(data, code);
    }

    public StringResponse(String data) {
        super(data);
    }

    public StringResponse(ServerResponseCode code) {
        super(code);
    }
}
