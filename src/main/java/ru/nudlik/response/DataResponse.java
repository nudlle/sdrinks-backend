package ru.nudlik.response;

import java.io.Serializable;
import java.util.Objects;

public abstract class DataResponse<T> implements Serializable {
    private static final long serialVersionUID = -821341241L;
    private T data;
    private ServerResponseCode code;

    public DataResponse() {
    }

    public DataResponse(T data, ServerResponseCode code) {
        this.data = data;
        this.code = code;
    }
    public DataResponse(T data) {
        this(data, ServerResponseCode.OK_RESPONSE);
    }
    public DataResponse(ServerResponseCode code) {
        this(null, code);
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public ServerResponseCode getCode() {
        return code;
    }

    public void setCode(ServerResponseCode code) {
        this.code = code;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DataResponse<?> that = (DataResponse<?>) o;
        return Objects.equals(data, that.data) &&
                code == that.code;
    }

    @Override
    public int hashCode() {
        return Objects.hash(data, code);
    }

    @Override
    public String toString() {
        return "DataResponse{" +
                "data=" + data +
                ", code=" + code +
                '}';
    }
}
