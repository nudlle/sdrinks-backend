package ru.nudlik.repository;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import reactor.core.publisher.Flux;
import ru.nudlik.domain.UserMessage;

import java.util.Date;

public interface UserMessageRepository extends ReactiveMongoRepository<UserMessage, String>, UserMessageRepositoryCustom {
    Flux<UserMessage> findAllByDate(Date date);
    Flux<UserMessage> findAllByUserId(String userId);
    Flux<UserMessage> findAllByAppVersion(String appVersion);
}
