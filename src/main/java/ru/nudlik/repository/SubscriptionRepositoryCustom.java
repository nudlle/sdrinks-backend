package ru.nudlik.repository;

import reactor.core.publisher.Mono;

public interface SubscriptionRepositoryCustom {
    Mono<Void> updateFields(String id, String [] fields, Object [] values);
}
