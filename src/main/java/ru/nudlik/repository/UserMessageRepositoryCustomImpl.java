package ru.nudlik.repository;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import reactor.core.publisher.Mono;
import ru.nudlik.domain.UserMessage;

public class UserMessageRepositoryCustomImpl implements UserMessageRepositoryCustom {
    @Autowired
    private ReactiveMongoTemplate mongoTemplate;

    @Override
    public Mono<Void> updateFields(String id, String[] fields, Object[] vals) {
        if (fields == null || vals == null) {
            throw new NullPointerException("Updated fields or values is null");
        }
        if (fields.length != vals.length) {
            throw new IllegalArgumentException("fields.length is not equals values.length: " + fields.length + " != " + vals.length);
        }
        Criteria criteria = Criteria.where("_id").is(new ObjectId(id));
        Query query = new Query(criteria);
        Update update = new Update();
        for (int i = 0; i < fields.length; i++) {
            update.set(fields[i], vals[i]);
        }
        return mongoTemplate.updateFirst(query, update, UserMessage.class).then();
    }
}
