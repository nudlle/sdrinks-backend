package ru.nudlik.repository;

import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import reactor.core.publisher.Flux;
import ru.nudlik.domain.Subscription;

import java.util.Date;

public interface SubscriptionRepository extends ReactiveMongoRepository<Subscription, String>, SubscriptionRepositoryCustom {
    Flux<Subscription> findAllByUserIdOrderByEndDate(String userId);
    Flux<Subscription> deleteByUserId(String userId);
    @Query("{ _id : { $exists: true }, 'userId': ?0, 'endDate': { $gt: ?1 }, 'activated': true }")
    Flux<Subscription> retrieveEnableValues(String userId, Date currentDate);
}
