package ru.nudlik.repository;

import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.nudlik.domain.UserComment;

public interface CommentRepository extends ReactiveMongoRepository<UserComment, String> {
    @Query(value = "{ _id : { $exists: true }, 'recipeId': ?0 }")
    Flux<UserComment> retrieveCommentPageableWithRecipeId(String recipeId, Pageable pageable);
    Mono<UserComment> findByRecipeIdAndUserId(String recipeId, String userId);
}
