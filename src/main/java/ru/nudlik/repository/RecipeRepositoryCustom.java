package ru.nudlik.repository;

import org.springframework.data.domain.Pageable;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.nudlik.domain.Recipe;
import ru.nudlik.domain.RecipesToSearch;

public interface RecipeRepositoryCustom {
    Mono<Void> updateFields(String id, String [] fields, Object [] values);
    Mono<Void> updateRecipeStepImageUrl(String id, String url);
    Mono<Void> updateRecipeAdImageUrl(String id, String url);
    Mono<Void> updateRecipeStepsRecipeId(String stepId, String recipeId);
    Mono<Void> updateRecipeAdRecipeId(String adId, String recipeId);
    Flux<Recipe> searchRecipe(RecipesToSearch search, Pageable page);
    Mono<Void> incRecipeViewed(String id);
    Mono<Void> incRecipeAdClicked(String id);
    Mono<Void> incRecipeLiked(String id);
    Mono<Void> decRecipeLiked(String id);
    Flux<Recipe> aggregateRecipes(Pageable page);
}
