package ru.nudlik.repository;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.data.mongodb.core.aggregation.*;
import org.springframework.data.mongodb.core.query.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.nudlik.domain.Recipe;
import ru.nudlik.domain.RecipesToSearch;

public class RecipeRepositoryCustomImpl implements RecipeRepositoryCustom {
    @Autowired
    private ReactiveMongoTemplate mongoTemplate;

    @Override
    public Mono<Void> updateFields(String id, String[] fields, Object[] vals) {
        if (fields == null || vals == null) {
            throw new NullPointerException("Updated fields or values is null");
        }
        if (fields.length != vals.length) {
            throw new IllegalArgumentException("fields.length is not equals values.length: " + fields.length + " != " + vals.length);
        }
        Criteria criteria = Criteria.where("_id").is(new ObjectId(id));
        Query query = new Query(criteria);
        Update update = new Update();
        for (int i = 0; i < fields.length; i++) {
            update.set(fields[i], vals[i]);
        }
        return mongoTemplate.updateFirst(query, update, Recipe.class).then();
    }

    @Override
    public Mono<Void> updateRecipeStepImageUrl(String id, String url) {
        Criteria criteria = Criteria.where("recipeSteps._id").is(new ObjectId(id));
        Query query = new Query(criteria);
        Update update = new Update();
        update.set("recipeSteps.$.imageUrl", url);
        return mongoTemplate.updateFirst(query, update, Recipe.class).then();
    }

    @Override
    public Mono<Void> updateRecipeAdImageUrl(String id, String url) {
        Criteria criteria = Criteria.where("ad._id").is(new ObjectId(id));
        Query query = new Query(criteria);
        Update update = new Update();
        update.set("ad.imageUrl", url);
        return mongoTemplate.updateFirst(query, update, Recipe.class).then();
    }

    @Override
    public Mono<Void> updateRecipeStepsRecipeId(String stepId, String recipeId) {
        Criteria criteria = Criteria.where("recipeSteps._id").is(new ObjectId(stepId));
        Query query = new Query(criteria);
        Update update = new Update();
        update.set("recipeSteps.$.recipeId", recipeId);
        return mongoTemplate.updateFirst(query, update, Recipe.class).then();
    }

    @Override
    public Mono<Void> updateRecipeAdRecipeId(String adId, String recipeId) {
        Criteria criteria = Criteria.where("ad._id").is(new ObjectId(adId));
        Query query = new Query(criteria);
        Update update = new Update();
        update.set("ad.recipeId", recipeId);
        return mongoTemplate.updateFirst(query, update, Recipe.class).then();
    }

    @Override
    public Flux<Recipe> searchRecipe(RecipesToSearch search, Pageable page) {
        TextCriteria criteria = TextCriteria.forDefaultLanguage()
                .matchingAny(search.getSearch()).caseSensitive(false);
        Query query = TextQuery.queryText(criteria)
                .sortByScore()
                .with(page);
        query.addCriteria(Criteria.where("enable").is(true));

        return mongoTemplate.find(query, Recipe.class);
    }

    @Override
    public Mono<Void> incRecipeViewed(String id) {
        Criteria criteria = Criteria.where("_id").is(new ObjectId(id));
        Query query = new Query(criteria);
        Update update = new Update();
        update.inc("viewed");
        return mongoTemplate.updateFirst(query, update, Recipe.class).then();
    }

    @Override
    public Mono<Void> incRecipeAdClicked(String id) {
        Criteria criteria = Criteria.where("ad._id").is(new ObjectId(id));
        Query query = new Query(criteria);
        Update update = new Update();
        update.inc("ad.clicked");
        return mongoTemplate.updateFirst(query, update, Recipe.class).then();
    }

    @Override
    public Mono<Void> incRecipeLiked(String id) {
        Criteria criteria = Criteria.where("_id").is(new ObjectId(id));
        Query query = new Query(criteria);
        Update update = new Update();
        update.inc("liked");
        return mongoTemplate.updateFirst(query, update, Recipe.class).then();
    }

    @Override
    public Mono<Void> decRecipeLiked(String id) {
        Criteria criteria = Criteria.where("_id").is(new ObjectId(id));
        Query query = new Query(criteria);
        Update update = new Update();
        update.inc("liked", -1);
        return mongoTemplate.updateFirst(query, update, Recipe.class).then();
    }

    @Override
    public Flux<Recipe> aggregateRecipes(Pageable page) {
        SampleOperation operation = Aggregation.sample(page.getPageSize());
        //SkipOperation skip = Aggregation.skip((long)page.getPageNumber() * page.getPageSize());
        MatchOperation match = Aggregation.match(Criteria.where("enable").is(true));
        Aggregation aggregation =
                Aggregation.newAggregation(match, operation);
        return mongoTemplate.aggregate(aggregation, "recipe", Recipe.class);
    }
}
