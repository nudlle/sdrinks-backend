package ru.nudlik.repository;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import reactor.core.publisher.Mono;
import ru.nudlik.domain.Subscription;
import ru.nudlik.domain.User;

public class UserRepositoryCustomImpl implements UserRepositoryCustom {
    @Autowired
    private ReactiveMongoTemplate mongoTemplate;

    @Override
    public Mono<Void> updateFields(String id, String[] fields, Object[] vals) {
        if (fields == null || vals == null) {
            throw new NullPointerException("Updated fields or values is null");
        }
        if (fields.length != vals.length) {
            throw new IllegalArgumentException("fields.length is not equals values.length: " + fields.length + " != " + vals.length);
        }
        Criteria criteria = Criteria.where("_id").is(new ObjectId(id));
        Query query = new Query(criteria);
        Update update = new Update();
        for (int i = 0; i < fields.length; i++) {
            update.set(fields[i], vals[i]);
        }
        return mongoTemplate.updateFirst(query, update, User.class).then();
    }

    @Override
    public Mono<Void> pushLikedRecipe(String id, String recipeId) {
        Criteria criteria = Criteria.where("_id").is(new ObjectId(id));
        Query query = new Query(criteria);
        Update update = new Update();
        update.addToSet("likedRecipes", recipeId);
        return mongoTemplate.updateFirst(query, update, User.class).then();
    }

    @Override
    public Mono<Void> pullLikedRecipe(String id, String recipeId) {
        Criteria criteria = Criteria.where("_id").is(new ObjectId(id));
        Query query = new Query(criteria);
        Update update = new Update();
        update.pull("likedRecipes", recipeId);
        return mongoTemplate.updateFirst(query, update, User.class).then();
    }

    @Override
    public Mono<Void> pushOwnRecipe(String id, String recipeId) {
        Criteria criteria = Criteria.where("_id").is(new ObjectId(id));
        Query query = new Query(criteria);
        Update update = new Update();
        update.addToSet("ownRecipes", recipeId);
        return mongoTemplate.updateFirst(query, update, User.class).then();
    }

    @Override
    public Mono<Void> pullOwnRecipe(String id, String recipeId) {
        Criteria criteria = Criteria.where("_id").is(new ObjectId(id));
        Query query = new Query(criteria);
        Update update = new Update();
        update.pull("ownRecipes", recipeId);
        return mongoTemplate.updateFirst(query, update, User.class).then();
    }

    @Override
    public Mono<Void> putSubscription(Subscription subscription) {
        Criteria criteria = Criteria.where("_id").is(new ObjectId(subscription.getUserId()));
        Query query = new Query(criteria);
        Update update = new Update();
        update.set("userSubscription.subscriptionId", subscription.getSubscriptionId());
        update.set("userSubscription.endDate", subscription.getEndDate());
        update.set("userSubscription.price", subscription.getPrice());
        update.set("userSubscription.userId", subscription.getUserId());
        update.set("userSubscription.activated", subscription.isActivated());
        return mongoTemplate.updateFirst(query, update, User.class).then();
    }

    @Override
    public Mono<Void> activateSubscription(String id) {
        Criteria criteria = Criteria.where("userSubscription._id").is(new ObjectId(id));
        Query query = new Query(criteria);
        Update update = new Update();
        update.set("userSubscription.activated", true);
        return mongoTemplate.updateFirst(query, update, User.class).then();
    }
}
