package ru.nudlik.repository;

import reactor.core.publisher.Mono;
import ru.nudlik.domain.Subscription;

public interface UserRepositoryCustom {
    Mono<Void> updateFields(String id, String [] fields, Object [] values);
    Mono<Void> pushLikedRecipe(String id, String recipeId);
    Mono<Void> pullLikedRecipe(String id, String recipeId);
    Mono<Void> pushOwnRecipe(String id, String recipeId);
    Mono<Void> pullOwnRecipe(String id, String recipeId);
    Mono<Void> putSubscription(Subscription subscription);
    Mono<Void> activateSubscription(String id);
}
