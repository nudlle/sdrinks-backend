package ru.nudlik.repository;

import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import reactor.core.publisher.Flux;
import ru.nudlik.domain.DrinkType;
import ru.nudlik.domain.Recipe;

public interface RecipeRepository extends ReactiveMongoRepository<Recipe, String>, RecipeRepositoryCustom {
    @Query("{ _id : { $exists: true }, userId: ?0 }")
    Flux<Recipe> retrieveByUserIdPaged(String userId, Pageable page);
    @Query("{ _id : { $exists: true }, drinkTypes: { $in: ?0 } }")
    Flux<Recipe> retrieveByDrinkTypePaged(DrinkType drinkType, Pageable page);
    @Query("{ _id : { $exists: true }, userEmail: { $regex: ?0 } }")
    Flux<Recipe> retrieveByUserEmailPaged(String email, Pageable page);
    @Query("{ _id : { $exists: true } }")
    Flux<Recipe> retrieveRecipesPaged(Pageable page);
    Flux<Recipe> findAllByEnable(boolean enable);
}
