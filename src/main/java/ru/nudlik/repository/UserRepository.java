package ru.nudlik.repository;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import reactor.core.publisher.Mono;
import ru.nudlik.domain.User;

public interface UserRepository extends ReactiveMongoRepository<User, String>, UserRepositoryCustom {
    Mono<User> findByEmail(String email);
}
