package ru.nudlik.repository;

import reactor.core.publisher.Mono;

public interface UserMessageRepositoryCustom {
    Mono<Void> updateFields(String id, String [] fields, Object [] values);
}
