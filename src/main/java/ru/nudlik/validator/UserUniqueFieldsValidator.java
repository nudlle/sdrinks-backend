package ru.nudlik.validator;

import reactor.core.publisher.Mono;
import ru.nudlik.domain.User;
import ru.nudlik.exceptions.EmailUniqueValidationException;
import ru.nudlik.repository.UserRepository;

public class UserUniqueFieldsValidator {
    private UserRepository repository;

    public UserUniqueFieldsValidator(UserRepository repository) {
        this.repository = repository;
    }

    public Mono<User> validateMono(User account) {
        return repository.findByEmail(account.getEmail())
                .doOnNext(a -> {
                    if (a.getEmail().equals(account.getEmail())) {
                        throw new EmailUniqueValidationException("This email " + a.getEmail() + " is already exists");
                    }
                })
                .switchIfEmpty(Mono.just(account));
    }
}
