package ru.nudlik.security;

import org.springframework.security.core.userdetails.ReactiveUserDetailsPasswordService;
import org.springframework.security.core.userdetails.ReactiveUserDetailsService;
import reactor.core.publisher.Mono;

public interface CustomUserDetailService extends ReactiveUserDetailsService, ReactiveUserDetailsPasswordService {
    Mono<SessionToken> addUserDetails(SessionToken st, String... roles);
    Mono<Void> deleteUserDetails(String username);
}
