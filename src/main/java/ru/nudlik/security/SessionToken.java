package ru.nudlik.security;

import java.io.Serializable;
import java.util.Base64;
import java.util.Objects;
import java.util.UUID;

public class SessionToken implements Serializable {
    private final static long serialVersionUID = 122123214010234L;
    private String id;
    private String token;
    private long timestamp;

    public SessionToken(String id) {
        this.id = id;
        this.timestamp = System.currentTimeMillis();
        this.token = Base64.getEncoder().encodeToString((id + timestamp).getBytes());
    }

    public SessionToken(SessionToken st) {
        this.id = st.id;
        this.timestamp = st.timestamp;
        this.token = st.token;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public long getTimestamp() {
        return timestamp;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SessionToken that = (SessionToken) o;
        return timestamp == that.timestamp &&
                Objects.equals(id, that.id) &&
                Objects.equals(token, that.token);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, token, timestamp);
    }

    @Override
    public String toString() {
        return "SessionToken{" +
                "id='" + id + '\'' +
                ", token='" + token + '\'' +
                ", timestamp=" + timestamp +
                '}';
    }
}
