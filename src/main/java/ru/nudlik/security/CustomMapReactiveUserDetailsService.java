package ru.nudlik.security;

import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.util.Assert;
import reactor.core.publisher.Mono;

import java.util.Arrays;
import java.util.Collection;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class CustomMapReactiveUserDetailsService implements CustomUserDetailService {
    private final Map<String, UserDetails> users;

    private static UserDetails getDefaultAdmin() {
        PasswordEncoder encoder = PasswordEncoderFactories.createDelegatingPasswordEncoder();
        return User.withUsername("admin").password(encoder.encode("nimda123")).roles("ADMIN").build();
    }

    public CustomMapReactiveUserDetailsService() {
        this(getDefaultAdmin());
    }

    public CustomMapReactiveUserDetailsService(Map<String, UserDetails> users) {
        this.users = users;
    }

    public CustomMapReactiveUserDetailsService(UserDetails... users) {
        this(Arrays.asList(users));
    }

    private CustomMapReactiveUserDetailsService(Collection<UserDetails> users) {
        Assert.notEmpty(users, "users cannot be null or empty");
        this.users = users.stream().collect(Collectors.toConcurrentMap(u -> getKey(u.getUsername()), Function.identity()));
    }

    @Override
    public Mono<UserDetails> findByUsername(String username) {
        String key = getKey(username);
        UserDetails result = users.get(key);
        return result == null ? Mono.empty() : Mono.just(User.withUserDetails(result).build());
    }

    @Override
    public Mono<UserDetails> updatePassword(UserDetails user, String newPassword) {
        return Mono.just(user)
                .map(u ->
                        User.withUserDetails(u)
                                .password(newPassword)
                                .build()
                )
                .doOnNext(u -> {
                    String key = getKey(user.getUsername());
                    this.users.put(key, u);
                });
    }

    private String getKey(String username) {
        return username.toLowerCase();
    }

    @Override
    public Mono<SessionToken> addUserDetails(SessionToken st, String... roles) {
        return Mono.just(st).map(u -> {
            PasswordEncoder encoder = PasswordEncoderFactories.createDelegatingPasswordEncoder();
            return User.withUsername(u.getId()).password(encoder.encode(u.getToken())).roles(roles).build();
        }).map(ud -> {
            this.users.put(getKey(ud.getUsername()), ud);
            return st;
        });
    }

    @Override
    public Mono<Void> deleteUserDetails(String username) {
        return Mono.just(username).map(this.users::remove).then();
    }
}
