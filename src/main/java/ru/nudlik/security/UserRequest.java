package ru.nudlik.security;

import ru.nudlik.domain.User;

import java.io.Serializable;
import java.util.Objects;

public class UserRequest implements Serializable {
    private static final long serialVersionUID = -123400038833L;
    private User user;
    private long timestamp;

    public UserRequest() {
    }
    public UserRequest(User user, long timestamp) {
        this.user = user;
        this.timestamp = timestamp;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserRequest that = (UserRequest) o;
        return timestamp == that.timestamp &&
                Objects.equals(user, that.user);
    }

    @Override
    public int hashCode() {
        return Objects.hash(user, timestamp);
    }

    @Override
    public String toString() {
        return "UserRequest{" +
                "user=" + user +
                ", timestamp=" + timestamp +
                '}';
    }
}
